/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.longhh.chat.di.component;

import com.android.longhh.chat.di.PerActivity;
import com.android.longhh.chat.ui.home.HomeFragment;
import com.android.longhh.chat.ui.home.homefriend.HomeFriendFragment;
import com.android.longhh.chat.ui.home.homefriend.tabfriend.TabFriendFragment;
import com.android.longhh.chat.ui.home.homefriend.tabrequest.TabRequestFragment;
import com.android.longhh.chat.ui.home.homefriend.tabuser.TabUserFragment;
import com.android.longhh.chat.ui.home.homemessenger.HomeMessengerFragment;
import com.android.longhh.chat.ui.home.homeprofile.HomeProfileFragment;
import com.android.longhh.chat.ui.home.homeprofile.changeprofile.ChangeProfileFragment;
import com.android.longhh.chat.ui.home.messenger.MessengerFragment;
import com.android.longhh.chat.ui.register.RegisterFragment;
import com.android.longhh.chat.ui.splash.SplashFragment;
import com.android.longhh.chat.di.module.ActivityModule;
import com.android.longhh.chat.ui.about.AboutFragment;
import com.android.longhh.chat.ui.feed.FeedActivity;
import com.android.longhh.chat.ui.feed.blogs.BlogFragment;
import com.android.longhh.chat.ui.feed.opensource.OpenSourceFragment;
import com.android.longhh.chat.ui.login.LoginFragment;
import com.android.longhh.chat.ui.main.MainActivity;

import dagger.Component;

/**
 * Created by janisharali on 27/01/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(SplashFragment fragment);

    void inject(RegisterFragment fragment);

    void inject(LoginFragment fragment);

    void inject(HomeFragment fragment);

    void inject(HomeMessengerFragment fragment);

    void inject(HomeFriendFragment fragment);

    void inject(HomeProfileFragment fragment);

    void inject(ChangeProfileFragment fragment);

    void inject(TabFriendFragment fragment);

    void inject(TabUserFragment fragment);

    void inject(TabRequestFragment fragment);

    void inject(FeedActivity activity);

    void inject(AboutFragment fragment);

    void inject(OpenSourceFragment fragment);

    void inject(BlogFragment fragment);

    void inject(MessengerFragment fragment);

}
