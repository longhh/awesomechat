package com.android.longhh.chat.ui.home.homefriend;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.ui.home.homefriend.tabfriend.TabFriendFragment;
import com.android.longhh.chat.ui.home.homefriend.tabrequest.TabRequestFragment;
import com.android.longhh.chat.ui.home.homefriend.tabuser.TabUserFragment;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFriendFragment extends BaseFragment implements HomeFriendMvpView{
    public static final String TAG = HomeFriendFragment.class.getSimpleName();

    @BindView(R.id.tab_layout_friend)
    TabLayout tabLayoutFriend;

    @BindView(R.id.view_pager_friend)
    ViewPager viewPagerFriend;

    @BindView(R.id.search_view_home_friend)
    EditText searchViewHomeFriend;

    @BindView(R.id.txt_request_number)
    TextView txtRequestNumber;


    @Inject
    HomeFriendMvpPresenter<HomeFriendMvpView> mPresenter;

    public static HomeFriendFragment newInstance() {
        Bundle args = new Bundle();
        HomeFriendFragment fragment = new HomeFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_friend, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void readRequest(ArrayList<Users> userArrayList) {
        if (userArrayList.size() == 0){
            txtRequestNumber.setVisibility(View.GONE);
        }else {
            txtRequestNumber.setVisibility(View.VISIBLE);
            txtRequestNumber.setText(String.valueOf(userArrayList.size()));
        }
    }

    public OnTextChanged onTextChanged;

    public interface OnTextChanged {
        void onTextChanged(String word);
    }

    public void setOnTextChanged (OnTextChanged onTextChanged) {
        this.onTextChanged = onTextChanged;
    }

    public OnTextChanged2 onTextChanged2;

    public interface OnTextChanged2 {
        void onTextChanged2(String word);
    }

    public void setOnTextChanged2 (OnTextChanged2 onTextChanged2) {
        this.onTextChanged2 = onTextChanged2;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.readRequest();
        searchViewHomeFriend.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (viewPagerFriend.getCurrentItem() == 0) {
                    if (onTextChanged != null) {
                        onTextChanged.onTextChanged(charSequence.toString().toLowerCase());
                    }
                } else if (viewPagerFriend.getCurrentItem() == 1) {
                    if (onTextChanged2 != null) {
                        onTextChanged2.onTextChanged2(charSequence.toString().toLowerCase());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(TabFriendFragment.newInstance(), getContext().getString(R.string.tab_friend));
        adapter.addFragment(TabUserFragment.newInstance(), getContext().getString(R.string.tab_all));
        adapter.addFragment(TabRequestFragment.newInstance(), getContext().getString(R.string.tab_request));
        viewPagerFriend.setAdapter(adapter);
        tabLayoutFriend.setupWithViewPager(viewPagerFriend);
    }
}
