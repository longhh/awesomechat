package com.android.longhh.chat.ui.home.homefriend.tabrequest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import java.util.ArrayList;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TabRequestFragment extends BaseFragment implements TabRequestMvpView {

    public static final String TAG = TabRequestFragment.class.getSimpleName();

    @Inject
    TabRequestPresenter<TabRequestMvpView> mPresenter;

    private ReceiverAdapter adapterReceiver;
    private SenderAdapter adapterSender;

    @BindView(R.id.rv_request_friend)
    RecyclerView rvRequestFriend;

    @BindView(R.id.rv_send_request_friend)
    RecyclerView rvSendRequestFriend;

    public static TabRequestFragment newInstance(Bundle args) {
        TabRequestFragment fragment = new TabRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static TabRequestFragment newInstance() {
        Bundle args = new Bundle();
        TabRequestFragment fragment = new TabRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_request, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        initAdapter();
        mPresenter.readSender();
        mPresenter.readReceiver();
    }
    private void initAdapter(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvRequestFriend.setLayoutManager(linearLayoutManager);
        adapterReceiver = new ReceiverAdapter(getContext(), new ArrayList<>());
        rvRequestFriend.setAdapter(adapterReceiver);

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext());
        rvSendRequestFriend.setLayoutManager(linearLayoutManager1);
        adapterSender = new SenderAdapter(getContext(), new ArrayList<>());
        rvSendRequestFriend.setAdapter(adapterSender);

        adapterReceiver.setOnClickItem(new ReceiverAdapter.IOnClickItem() {
            @Override
            public void itemClick(Users users) {
                mPresenter.agreeRequest(users);
            }

            @Override
            public void deleteItem(Users users) {
                mPresenter.disagreeRequest(users);
            }
        });

        adapterSender.setOnClickItemSender(new SenderAdapter.IOnClickItemSender() {
            @Override
            public void itemClick(Users users) {
                mPresenter.cancelRequest(users);
            }
        });
    }

    @Override
    public void readSender(ArrayList<Users> users) {
        adapterSender.setData(users);
    }

    @Override
    public void readReceiver(ArrayList<Users> users) {

        adapterReceiver.setData(users);
    }
}
