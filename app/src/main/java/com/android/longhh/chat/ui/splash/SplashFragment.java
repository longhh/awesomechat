package com.android.longhh.chat.ui.splash;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.res.ResourcesCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.longhh.chat.R;
import com.android.longhh.chat.ui.home.HomeFragment;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.ui.login.LoginFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashFragment extends BaseFragment implements SplashMvpView {

    public static final String TAG = SplashFragment.class.getSimpleName();

    @BindView(R.id.textViewAwesome)
    AppCompatTextView textViewAwesome;
    @BindView(R.id.textViewChat)
    AppCompatTextView textViewChat;

    @Inject
    SplashMvpPresenter<SplashMvpView> mPresenter;

    public static SplashFragment newInstance() {
        Bundle args = new Bundle();
        SplashFragment fragment = new SplashFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_splash, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        Typeface typeBold = ResourcesCompat.getFont(getContext(), R.font.exo_black);
        textViewAwesome.setTypeface(typeBold);
        Typeface typeThin = ResourcesCompat.getFont(getContext(), R.font.exo_thin);
        textViewChat.setTypeface(typeThin);
    }

    @Override
    public void openLoginFragment() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameContainer, LoginFragment.newInstance(), LoginFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void openHomeFragment() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameContainer, HomeFragment.newInstance(), HomeFragment.TAG)
                    .commit();
        }
    }
}
