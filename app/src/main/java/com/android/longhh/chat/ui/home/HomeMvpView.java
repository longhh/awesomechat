package com.android.longhh.chat.ui.home;

import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface HomeMvpView extends MvpView {
    void readRooms(Integer count);
    void readRequest(ArrayList<Users> users);
}
