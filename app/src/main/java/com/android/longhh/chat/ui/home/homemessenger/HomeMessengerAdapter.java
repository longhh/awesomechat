package com.android.longhh.chat.ui.home.homemessenger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.DateUtils;
import com.bumptech.glide.Glide;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeMessengerAdapter extends RecyclerView.Adapter<HomeMessengerAdapter.MyViewHolder> {
    private final Context context;
    private ArrayList<Room> data;
    public ClickItemView clickItemView;

    public HomeMessengerAdapter(Context context, ArrayList<Room> data) {
        this.context = context;
        this.data = data;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(ArrayList<Room> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ArrayList<Room> getData() {
        return data;
    }

    public interface ClickItemView {
        void onClickItem (Room room, int position);
    }

    public void setOnItemClick (ClickItemView clickItemView) {
        this.clickItemView = clickItemView;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_home_messenger, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Room room = data.get(i);
        try {
            myViewHolder.bindView(room);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @BindView(R.id.txt_name)
        TextView txtUsername;

        @BindView(R.id.img_circle_home_mess)
        CircleImageView imgCircleHomeMess;

        @BindView(R.id.item_home_messenger)
        ConstraintLayout itemHomeMessenger;

        @BindView(R.id.txt_unread_message)
        TextView txtUnreadMessage;

        @BindView(R.id.txt_last_message)
        TextView txtLastMessage;

        @BindView(R.id.txt_time)
        TextView txtTime;


        public void bindView(Room room) throws ParseException {
            Typeface typeNormal = ResourcesCompat.getFont(context, R.font.lato_normal);
            Typeface typeBold = ResourcesCompat.getFont(context, R.font.lato_bold);
            txtUsername.setTypeface(typeBold);
            txtUsername.setText(room.getName());

            DateUtils current = new DateUtils(System.currentTimeMillis());
            DateUtils time = new DateUtils(Long.parseLong(room.getTimestamp()));

            int results = Integer.parseInt(current.getDayLong()) - Integer.parseInt(time.getDayLong());
            if (current.getMonthLong().equals(time.getMonthLong())){
                if (results == 1) {
                    txtTime.setText(context.getString(R.string.yesterday));
                } else if (results > 1) {
                    txtTime.setText(time.getDateLong());
                } else {
                    txtTime.setText(time.getHourLong());
                }
            } else {
                txtTime.setText(time.getDateLong());
            }

            if (room.getUnreadMessage() > 0) {
                txtLastMessage.setTypeface(typeBold);
                txtUnreadMessage.setVisibility(View.VISIBLE);
                if (room.getUnreadMessage() > 9) {
                    txtUnreadMessage.setText("+9");
                } else {
                    txtUnreadMessage.setText(String.valueOf(room.getUnreadMessage()));
                }
            } else {
                txtLastMessage.setTypeface(typeNormal);
                txtUnreadMessage.setVisibility(View.GONE);
            }
            txtLastMessage.setText(room.getLastMessage());
            if (room.getThumbnail().equals(AppConstants.FIRE_BASE_DEFAULT)) {
                Glide.with(context).load(R.drawable.ic_avt).into(imgCircleHomeMess);
            } else {
                Glide.with(context).load(room.getThumbnail()).into(imgCircleHomeMess);
            }
            itemHomeMessenger.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickItemView != null) {
                        clickItemView.onClickItem(room, getAdapterPosition());
                    }
                }
            });
        }
    }
}