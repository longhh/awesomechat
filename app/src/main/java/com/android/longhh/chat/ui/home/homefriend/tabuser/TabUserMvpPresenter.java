package com.android.longhh.chat.ui.home.homefriend.tabuser;

import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.MvpPresenter;
import com.android.longhh.chat.ui.base.MvpView;

public interface TabUserMvpPresenter <V extends TabUserMvpView & MvpView> extends MvpPresenter<V> {
    void sendRequest(Users User);

    void cancelRequest(Users User);

    void searchUser(String word);
}
