package com.android.longhh.chat.ui.home.homemessenger;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.ui.home.messenger.MessengerFragment;
import com.android.longhh.chat.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeMessengerFragment extends BaseFragment implements HomeMessengerMvpView {
    public static final String TAG = HomeMessengerFragment.class.getSimpleName();

    private HomeMessengerAdapter adapter;

    @BindView(R.id.recycler_view_home_messenger)
    RecyclerView recyclerViewHomeMessenger;

    @BindView(R.id.search_view_mess)
    EditText searchViewMess;

    @BindView(R.id.img_new_mess)
    ImageButton imgNewMess;

    @Inject
    HomeMessengerMvpPresenter<HomeMessengerMvpView> mPresenter;

    public static HomeMessengerFragment newInstance() {
        Bundle args = new Bundle();
        HomeMessengerFragment fragment = new HomeMessengerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_messenger, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        initAdapter();
        searchViewMess.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPresenter.searchByName(charSequence.toString().toLowerCase());
//                    mPresenter.searchByMess(charSequence.toString().toLowerCase());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        imgNewMess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEventBus();
            }
        });
        recyclerViewHomeMessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }

    private void sendEventBus() {
        EventBus.getDefault().post(new EventBusMess(1));
    }

    private void initAdapter() {
        recyclerViewHomeMessenger.setHasFixedSize(true);
        recyclerViewHomeMessenger.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new HomeMessengerAdapter(getContext(), new ArrayList<>());
        recyclerViewHomeMessenger.setAdapter(adapter);
        adapter.setOnItemClick(new HomeMessengerAdapter.ClickItemView() {
            @Override
            public void onClickItem(Room room, int position) {
                openChatRoom(adapter.getData().get(position));
            }
        });
    }

    private void openChatRoom(Room room) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEY_ROOM, room);
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_left, R.anim.slide_right, R.anim.slide_left, R.anim.slide_right)
                    .add(R.id.frameContainer, MessengerFragment.newInstance(bundle), MessengerFragment.TAG)
                    .addToBackStack(MessengerFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void onSuccess(ArrayList<Room> roomArrayList) {
        if (adapter != null) {
            Collections.sort(roomArrayList, new Comparator<Room>() {
                @Override
                public int compare(Room room, Room t1) {
                    return Long.compare(Long.parseLong(t1.getTimestamp()), Long.parseLong(room.getTimestamp()));
                }
            });
            adapter.setData(roomArrayList);
        }
    }
}
