package com.android.longhh.chat.ui.home.messenger;

import com.android.longhh.chat.fcm.MyResponse;
import com.android.longhh.chat.fcm.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAA2eMiKuc:APA91bFkhQzsWTlDNxDqErW5A0NVIXt6CIvH_O7Vx4aFdgOTXWdJbADLWN605n-pc6IugyNsdxZ9q44FRYoyMW-kh_rAqC_sw4nzSF2wpHVaRG-1652z65HjHVOzJoawtAWZ4PbKIyPq"
            }
    )
    @POST("fcm/send")
    Call<MyResponse> sendNotifications(@Body Sender body);
}
