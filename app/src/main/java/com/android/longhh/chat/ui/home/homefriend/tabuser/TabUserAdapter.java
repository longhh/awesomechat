package com.android.longhh.chat.ui.home.homefriend.tabuser;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.android.longhh.chat.R;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.listener.OnItemClickListener;
import com.bumptech.glide.Glide;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TabUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private List<UserSort> usersList;
    public OnItemClickListener onItemClickListener;

    public static final int SECTION_VIEW = 1;
    public static final int CONTENT_VIEW = 2;

    public TabUserAdapter(Context context, List<UserSort> usersList) {
        this.context = context;
        this.usersList = usersList;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<UserSort> userList) {
        usersList = userList;
        notifyDataSetChanged();
    }

    void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i == SECTION_VIEW) {
            view = LayoutInflater.from(context).inflate(R.layout.item_tab_user_header, viewGroup, false);
            return new MyViewHolderHead(view);
        }
        view = LayoutInflater.from(context).inflate(R.layout.item_tab_user, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        UserSort userSort = usersList.get(i);
        switch (viewHolder.getItemViewType()) {
            case SECTION_VIEW:
                MyViewHolderHead myViewHolderHead = (MyViewHolderHead) viewHolder;
                myViewHolderHead.bindView(userSort);
                break;
            case CONTENT_VIEW:
                MyViewHolder myViewHolder = (MyViewHolder) viewHolder;
                myViewHolder.bindView(userSort, onItemClickListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (usersList.get(position).isSection()) {
            return SECTION_VIEW;
        } else {
            return CONTENT_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_username)
        AppCompatTextView txtUsername;
        @BindView(R.id.img_circle_friend)
        CircleImageView imgCircleFriend;
        @BindView(R.id.item_tab_friend)
        ConstraintLayout itemTabFriend;
        @BindView(R.id.btn_send_request_fr)
        AppCompatButton btnSendRequest;
        @BindView(R.id.btn_cancel_request_fr)
        AppCompatButton btnCancelRequest;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(UserSort user, OnItemClickListener onItemClickListener) {

            if (user.getStateUser() == AppConstants.StateUser.FRIEND) {
                btnCancelRequest.setVisibility(View.GONE);
                btnSendRequest.setVisibility(View.GONE);
            } else if (user.getStateUser() == AppConstants.StateUser.REQUEST_FRIEND) {
                btnCancelRequest.setVisibility(View.VISIBLE);
                btnSendRequest.setVisibility(View.GONE);
            } else {
                btnCancelRequest.setVisibility(View.GONE);
                btnSendRequest.setVisibility(View.VISIBLE);
            }
            Typeface typeBold = ResourcesCompat.getFont(context, R.font.lato_bold);
            txtUsername.setTypeface(typeBold);
            txtUsername.setText(user.getUsers().getUsername());
            if (user.getUsers().getImageURL().equals(AppConstants.FIRE_BASE_DEFAULT)) {
                Glide.with(context).load(R.drawable.ic_avt).into(imgCircleFriend);
            } else {
                Glide.with(context).load(user.getUsers().getImageURL()).into(imgCircleFriend);
            }
            btnSendRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnCancelRequest.setVisibility(View.VISIBLE);
                    btnSendRequest.setVisibility(View.GONE);
                    if (onItemClickListener != null) {
                        onItemClickListener.sendRequestFriend(getAdapterPosition());
                    }
                }
            });
            btnCancelRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnCancelRequest.setVisibility(View.GONE);
                    btnSendRequest.setVisibility(View.VISIBLE);
                    if (onItemClickListener != null) {
                        onItemClickListener.cancelRequestFriend(getAdapterPosition());
                    }
                }
            });

        }
    }

    public class MyViewHolderHead extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_header)
        TextView txtHeader;

        public MyViewHolderHead(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(UserSort user) {
            Typeface typeBold = ResourcesCompat.getFont(context, R.font.lato_bold);
            txtHeader.setTypeface(typeBold);
            txtHeader.setText(user.getHeader());
        }
    }
}

