package com.android.longhh.chat.ui.home.homefriend.tabrequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.utils.AppConstants;
import com.bumptech.glide.Glide;
import com.daimajia.swipe.SwipeLayout;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ReceiverAdapter extends RecyclerView.Adapter<ReceiverAdapter.ReceiverViewHolder>{

    private final Context mContext;
    private ArrayList<Users> data;

    public ReceiverAdapter(Context mContext, ArrayList<Users> data) {
        this.mContext = mContext;
        this.data = data;
    }
    public interface IOnClickItem{
        void itemClick(Users users);
        void deleteItem(Users users);
    }
    public IOnClickItem iOnClickItem;

    public void setOnClickItem(IOnClickItem iOnClickItem) {
        this.iOnClickItem = iOnClickItem;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(ArrayList<Users> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ReceiverViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_tab_request_agree, viewGroup, false);
        return new ReceiverAdapter.ReceiverViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceiverViewHolder receiverViewHolder, int i) {
        Users users = data.get(i);
        receiverViewHolder.bindView(users, iOnClickItem);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    class ReceiverViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_circle_friend)
        CircleImageView imgCircleFriend;

        @BindView(R.id.txt_username)
        AppCompatTextView txtUserName;

        @BindView(R.id.btn_agree_request)
        AppCompatButton btnAgreeRequest;

        @BindView(R.id.swipe_layout)
        SwipeLayout swipeLayout;

        public ReceiverViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        public void bindView(Users users, IOnClickItem iOnClickItem){
            Typeface typeBold = ResourcesCompat.getFont(mContext, R.font.lato_bold);
            txtUserName.setTypeface(typeBold);
            txtUserName.setText(users.getUsername());
            if (users.getImageURL().equals(AppConstants.FIRE_BASE_DEFAULT)) {
                Glide.with(mContext).load(R.drawable.ic_avt).circleCrop().into(imgCircleFriend);
            } else {
                Glide.with(mContext).load(users.getImageURL()).circleCrop().into(imgCircleFriend);
            }
            btnAgreeRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (iOnClickItem != null){
                        iOnClickItem.itemClick(users);
                    }
                }
            });
            swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
            swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeLayout.findViewById(R.id.layout_disagree));
            swipeLayout.findViewById(R.id.tv_disagree).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(iOnClickItem != null){
                        iOnClickItem.deleteItem(users);
                    }
                }
            });
        }
    }
}
