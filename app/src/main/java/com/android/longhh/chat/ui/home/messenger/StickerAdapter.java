package com.android.longhh.chat.ui.home.messenger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.longhh.chat.R;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StickerAdapter extends RecyclerView.Adapter<StickerAdapter.MyViewHolder>{
    private Context context;
    private ArrayList<String> list;
    public ClickItemSticker clickItemSticker;

    public StickerAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
    }

    public interface ClickItemSticker {
        void onClickItemSticker (String sticker, int position);
    }

    public void setOnItemStickerClick (ClickItemSticker clickItemSticker) {
        this.clickItemSticker = clickItemSticker;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_select_sticker, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        String sticker = list.get(i);
        Glide.with(context).load(sticker).into(myViewHolder.imgFromListSticker);
        myViewHolder.bindView(sticker);
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        } else {
            return list.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_form_list_sticker)
        ImageView imgFromListSticker;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(String sticker){
            imgFromListSticker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickItemSticker != null) {
                        clickItemSticker.onClickItemSticker(sticker, getAdapterPosition());
                    }
                }
            });
        }
    }
}
