/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.longhh.chat.di.module;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.longhh.chat.di.ActivityContext;
import com.android.longhh.chat.di.PerActivity;
import com.android.longhh.chat.ui.home.HomeMvpPresenter;
import com.android.longhh.chat.ui.home.HomeMvpView;
import com.android.longhh.chat.ui.home.HomePresenter;
import com.android.longhh.chat.data.network.model.BlogResponse;
import com.android.longhh.chat.data.network.model.OpenSourceResponse;
import com.android.longhh.chat.ui.about.AboutMvpPresenter;
import com.android.longhh.chat.ui.about.AboutMvpView;
import com.android.longhh.chat.ui.about.AboutPresenter;
import com.android.longhh.chat.ui.feed.FeedMvpPresenter;
import com.android.longhh.chat.ui.feed.FeedMvpView;
import com.android.longhh.chat.ui.feed.FeedPagerAdapter;
import com.android.longhh.chat.ui.feed.FeedPresenter;
import com.android.longhh.chat.ui.feed.blogs.BlogAdapter;
import com.android.longhh.chat.ui.feed.blogs.BlogMvpPresenter;
import com.android.longhh.chat.ui.feed.blogs.BlogMvpView;
import com.android.longhh.chat.ui.feed.blogs.BlogPresenter;
import com.android.longhh.chat.ui.feed.opensource.OpenSourceAdapter;
import com.android.longhh.chat.ui.feed.opensource.OpenSourceMvpPresenter;
import com.android.longhh.chat.ui.feed.opensource.OpenSourceMvpView;
import com.android.longhh.chat.ui.feed.opensource.OpenSourcePresenter;
import com.android.longhh.chat.ui.home.homefriend.HomeFriendMvpPresenter;
import com.android.longhh.chat.ui.home.homefriend.HomeFriendMvpView;
import com.android.longhh.chat.ui.home.homefriend.HomeFriendPresenter;
import com.android.longhh.chat.ui.home.homefriend.tabfriend.TabFriendMvpPresenter;
import com.android.longhh.chat.ui.home.homefriend.tabfriend.TabFriendMvpView;
import com.android.longhh.chat.ui.home.homefriend.tabfriend.TabFriendPresenter;
import com.android.longhh.chat.ui.home.homefriend.tabrequest.TabRequestMvpPresenter;
import com.android.longhh.chat.ui.home.homefriend.tabrequest.TabRequestMvpView;
import com.android.longhh.chat.ui.home.homefriend.tabrequest.TabRequestPresenter;
import com.android.longhh.chat.ui.home.homefriend.tabuser.TabUserMvpPresenter;
import com.android.longhh.chat.ui.home.homefriend.tabuser.TabUserMvpView;
import com.android.longhh.chat.ui.home.homefriend.tabuser.TabUserPresenter;
import com.android.longhh.chat.ui.home.homemessenger.HomeMessengerMvpPresenter;
import com.android.longhh.chat.ui.home.homemessenger.HomeMessengerMvpView;
import com.android.longhh.chat.ui.home.homemessenger.HomeMessengerPresenter;
import com.android.longhh.chat.ui.home.homeprofile.HomeProfileMvpPresenter;
import com.android.longhh.chat.ui.home.homeprofile.HomeProfileMvpView;
import com.android.longhh.chat.ui.home.homeprofile.HomeProfilePresenter;
import com.android.longhh.chat.ui.home.homeprofile.changeprofile.ChangeProfileMvpPresenter;
import com.android.longhh.chat.ui.home.homeprofile.changeprofile.ChangeProfileMvpView;
import com.android.longhh.chat.ui.home.homeprofile.changeprofile.ChangeProfilePresenter;
import com.android.longhh.chat.ui.login.LoginMvpPresenter;
import com.android.longhh.chat.ui.login.LoginMvpView;
import com.android.longhh.chat.ui.login.LoginPresenter;
import com.android.longhh.chat.ui.main.MainMvpPresenter;
import com.android.longhh.chat.ui.main.MainMvpView;
import com.android.longhh.chat.ui.main.MainPresenter;
import com.android.longhh.chat.ui.home.messenger.MessengerMvpPresenter;
import com.android.longhh.chat.ui.home.messenger.MessengerMvpView;
import com.android.longhh.chat.ui.home.messenger.MessengerPresenter;
import com.android.longhh.chat.ui.register.RegisterMvpPresenter;
import com.android.longhh.chat.ui.register.RegisterMvpView;
import com.android.longhh.chat.ui.register.RegisterPresenter;
import com.android.longhh.chat.ui.splash.SplashMvpPresenter;
import com.android.longhh.chat.ui.splash.SplashMvpView;
import com.android.longhh.chat.ui.splash.SplashPresenter;
import com.android.longhh.chat.utils.rx.AppSchedulerProvider;
import com.android.longhh.chat.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    RegisterMvpPresenter<RegisterMvpView> provideRegisterPresenter(
            RegisterPresenter<RegisterMvpView> presenter) {
        return presenter;
    }

    @Provides
    HomeMvpPresenter<HomeMvpView> provideHomeMvpPresenter(
            HomePresenter<HomeMvpView> presenter) {
        return presenter;
    }

    @Provides
    HomeMessengerMvpPresenter<HomeMessengerMvpView> provideHomeMessengerMvpPresenter(
            HomeMessengerPresenter<HomeMessengerMvpView> presenter) {
        return presenter;
    }

    @Provides
    HomeFriendMvpPresenter<HomeFriendMvpView> provideHomeFriendMvpPresenter(
            HomeFriendPresenter<HomeFriendMvpView> presenter) {
        return presenter;
    }

    @Provides
    HomeProfileMvpPresenter<HomeProfileMvpView> provideHomeProfileMvpPresenter(
            HomeProfilePresenter<HomeProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    ChangeProfileMvpPresenter<ChangeProfileMvpView> provideChangeProfileMvpPresenter(
            ChangeProfilePresenter<ChangeProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    TabFriendMvpPresenter<TabFriendMvpView> provideTabFriendMvpPresenter(
            TabFriendPresenter<TabFriendMvpView> presenter) {
        return presenter;
    }

    @Provides
    TabUserMvpPresenter<TabUserMvpView> provideTabUserMvpPresenter(
            TabUserPresenter<TabUserMvpView> presenter) {
        return presenter;
    }

    @Provides
    TabRequestMvpPresenter<TabRequestMvpView> provideTabRequestMvpPresenter(
            TabRequestPresenter<TabRequestMvpView> presenter) {
        return presenter;
    }

    @Provides
    MessengerMvpPresenter<MessengerMvpView> provideMessengerMvpPresenter(
            MessengerPresenter<MessengerMvpView> presenter) {
        return presenter;
    }

    @Provides
    AboutMvpPresenter<AboutMvpView> provideAboutPresenter(
            AboutPresenter<AboutMvpView> presenter) {
        return presenter;
    }

    @Provides
    FeedMvpPresenter<FeedMvpView> provideFeedPresenter(
            FeedPresenter<FeedMvpView> presenter) {
        return presenter;
    }

    @Provides
    OpenSourceMvpPresenter<OpenSourceMvpView> provideOpenSourcePresenter(
            OpenSourcePresenter<OpenSourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    BlogMvpPresenter<BlogMvpView> provideBlogMvpPresenter(
            BlogPresenter<BlogMvpView> presenter) {
        return presenter;
    }

    @Provides
    FeedPagerAdapter provideFeedPagerAdapter(AppCompatActivity activity) {
        return new FeedPagerAdapter(activity.getSupportFragmentManager());
    }

    @Provides
    OpenSourceAdapter provideOpenSourceAdapter() {
        return new OpenSourceAdapter(new ArrayList<OpenSourceResponse.Repo>());
    }

    @Provides
    BlogAdapter provideBlogAdapter() {
        return new BlogAdapter(new ArrayList<BlogResponse.Blog>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
