/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.longhh.chat.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by amitshekhar on 08/01/17.
 */

public final class AppConstants {

    public static final String BUNDLE_KEY_ROOM = "ROOM";
    public static final String BUNDLE_KEY_USERS = "USERS";
    public static final String BUNDLE_KEY_NAME = "nameChanged";
    public static final String BUNDLE_KEY_IMAGE = "imageChanged";

    public static final String FIRE_BASE_DEFAULT = "default";
    public static final String FIRE_BASE_USERS = "Users";
    public static final String FIRE_BASE_ID = "id";
    public static final String FIRE_BASE_USERNAME = "username";
    public static final String FIRE_BASE_IMAGE_URL = "imageURL";
    public static final String FIRE_BASE_SEARCH = "search";
    public static final String FIRE_BASE_SEARCH_BY_NAME = "searchByName";
    public static final String FIRE_BASE_CONTENT = "content";

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String REFERENCE_USERS = "Users";
    public static final String REFERENCE_ROOMS = "Rooms";
    public static final String REFERENCE_MEMBERS = "Members";
    public static final String REFERENCE_MESSAGES = "Messages";
    public static final String REFERENCE_FRIENDS= "Friends";
    public static final String REFERENCE_TOKENS= "Tokens";
    public static final String REFERENCE_FRIEND_REQUEST = "friend_request";
    public static final String REFERENCE_REQUEST_SEND = "request_send";
    public static final String REFERENCE_REQUEST_RECEIVE = "request_receive";

    public static final String UNREAD_MESSAGE = "unreadMessage";

    public static final String DB_NAME = "mindorks_mvp.db";
    public static final String PREF_NAME = "mindorks_pref";

    public static final long NULL_INDEX = -1L;

    public static class TimeFormat {
        public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
        public static final String FULL = "dd-MM-yyyy HH:mm:ss";
        public static final String DAY = "dd";
        public static final String MONTH = "MM";
        public static final String DATE = "dd-MM-yyyy";
        public static final String HOURS = "HH:mm";
    }

    public static List stickers = Arrays.asList("https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_10.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_02.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_07.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_08.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_19.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_10.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_11.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_12.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_13.png");

    public enum StateUser {
        FRIEND,
        REQUEST_FRIEND,
        OTHERS
    }

    public enum AppLanguage {
        VIETNAMESE("vi", "Tiếng Việt"),
        ENGLISH("en", "English");

        String locale, language;

        AppLanguage(String locale, String language) {
            this.locale = locale;
            this.language = language;
        }

        public static String[] getListLanguage() {
            List<String> results = new ArrayList<>();
            for (AppConstants.AppLanguage language : values()) {
                results.add(language.getLanguage());
            }
            return results.toArray(new String[0]);
        }

        public static int getIndexLanguage(String locale) {
            List<AppLanguage> languages = Arrays.asList(values());
            for (AppConstants.AppLanguage language : languages) {
                if (language.locale.equals(locale)) {
                    return languages.indexOf(language);
                }
            }
            return -1;
        }

        public String getLocale() {
            return locale;
        }

        public String getLanguage() {
            return language;
        }
    }

    public static class Notification {
        public static final String USER = "user";
        public static final String ICON = "icon";
        public static final String TITLE = "title";
        public static final String BODY = "body";
        public static final String SENT = "sent";
    }

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
