package com.android.longhh.chat.ui.home.homemessenger;

import androidx.annotation.NonNull;
import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;

public class HomeMessengerPresenter<V extends HomeMessengerMvpView> extends BasePresenter<V>
        implements HomeMessengerMvpPresenter<V> {

    ArrayList<Room> roomArrayList;

    @Inject
    public HomeMessengerPresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_ROOMS).keepSynced(true);
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_MESSAGES).keepSynced(true);
        getListChatRoom();
    }

    private void getListChatRoom() {
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_ROOMS, getDataManager().getFirebaseUser().getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        roomArrayList = new ArrayList<>();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Room room = dataSnapshot.getValue(Room.class);
                            if (room != null && !room.getLastMessage().equals("")) {
                                roomArrayList.add(room);
                            }
                        }
                        getMvpView().onSuccess(roomArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        getMvpView().showMessage(error.getMessage());
                    }
                });
    }

    @Override
    public void searchByName(String name) {
        ArrayList<Room> arrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_ROOMS)
                .child(getDataManager().getFirebaseUserId())
                .orderByChild(AppConstants.FIRE_BASE_SEARCH_BY_NAME)
                .startAt(name).endAt(name + "\uf8ff")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        arrayList.clear();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Room room = dataSnapshot.getValue(Room.class);
                            assert room != null;
                            if (!room.getId().equals(getDataManager().getFirebaseUserId())
                                    && !room.getLastMessage().equals("")) {
                                arrayList.add(room);
                            }
                        }
                        getMvpView().onSuccess(arrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
}
