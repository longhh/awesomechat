package com.android.longhh.chat.ui.home.homeprofile.changeprofile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.utils.AppConstants;
import com.bumptech.glide.Glide;
import com.rengwuxian.materialedittext.MaterialEditText;
import java.util.Objects;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChangeProfileFragment extends BaseFragment implements ChangeProfileMvpView {

    public static final String TAG = ChangeProfileFragment.class.getSimpleName();
    public static final int IMAGE_REQUEST = 1;

    @BindView(R.id.txt_done)
    TextView txtDone;

    @BindView(R.id.img_camera)
    ImageView imgCamera;

    @BindView(R.id.img_back_home_profile)
    ImageView imgBack;

    @BindView(R.id.img_ava)
    CircleImageView imgAva;

    @BindView(R.id.edt_change_name)
    MaterialEditText edtChangeName;

    Uri mUri;

    @Inject
    ChangeProfileMvpPresenter<ChangeProfileMvpView> mPresenter;

    public static ChangeProfileFragment newInstance() {
        Bundle args = new Bundle();
        ChangeProfileFragment fragment = new ChangeProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_profile, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.setUpView();
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mUri != null){
                    mPresenter.uploadImage(mUri);
                }
                if (Objects.requireNonNull(edtChangeName.getText()).length() > 1) {
                    mPresenter.uploadName(edtChangeName.getText().toString());
                }
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMAGE_REQUEST);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            mUri = data.getData();
            if (mUri != null) {
                if (getContext() != null) {
                    if (mUri.toString().equals(AppConstants.FIRE_BASE_DEFAULT))
                        Glide.with(getContext()).load(R.drawable.ic_avt).into(imgAva);
                    else
                        Glide.with(getContext()).load(mUri.toString()).into(imgAva);
                }
            }
        }
    }

    @Override
    public void onSuccess(Users users) {
        if (getContext() != null) {
            edtChangeName.setText(users.getUsername());
            if (users.getImageURL().equals(AppConstants.FIRE_BASE_DEFAULT))
                Glide.with(getContext()).load(R.drawable.ic_avt).into(imgAva);
            else
                Glide.with(getContext()).load(users.getImageURL()).into(imgAva);
        }
    }
}