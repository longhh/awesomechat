package com.android.longhh.chat.ui.home.homefriend.tabfriend;

import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.MvpPresenter;

public interface TabFriendMvpPresenter<V extends TabFriendMvpView> extends MvpPresenter<V> {
    void readUser();

    void checkExistsRoom(Users receiver);

    void searchUser(String word);
}
