package com.android.longhh.chat.ui.login;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.longhh.chat.R;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.ui.home.HomeFragment;
import com.android.longhh.chat.ui.register.RegisterFragment;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment implements LoginMvpView {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;

    @BindView(R.id.edt_login_email)
    MaterialEditText edtLoginEmail;

    @BindView(R.id.edt_login_password)
    MaterialEditText edtLoginPassword;

    @BindView(R.id.txt_register)
    TextView txtRegister;

    @BindView(R.id.btn_login)
    Button btnLogin;


    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        changeColorButton();
    }

    @OnClick(R.id.btn_login)
    public void onLogin() {
        String email = Objects.requireNonNull(edtLoginEmail.getText()).toString();
        String password = Objects.requireNonNull(edtLoginPassword.getText()).toString();
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(getActivity(), getString(R.string.not_required), Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.onLogin(email, password);
        }
    }


    @Override
    public void successLogin() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameContainer, HomeFragment.newInstance(), HomeFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void fieldLogin() {
        Toast.makeText(getActivity(), getString(R.string.login_field), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.txt_register)
    @Override
    public void openRegisterFragment() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .add(R.id.frameContainer, RegisterFragment.newInstance(), RegisterFragment.TAG)
                    .addToBackStack(RegisterFragment.TAG)
                    .commit();
        }
    }

    private void changeColorButton() {
        edtLoginEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Objects.requireNonNull(edtLoginEmail.getText()).length() > 0
                        && Objects.requireNonNull(edtLoginPassword.getText()).length() > 0) {
                    btnLogin.setBackgroundResource(R.drawable.custom_button_blue);
                } else {
                    btnLogin.setBackgroundResource(R.drawable.custom_button_grey);
                }
            }
        });
        edtLoginPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Objects.requireNonNull(edtLoginEmail.getText()).length() > 0
                        && Objects.requireNonNull(edtLoginPassword.getText()).length() > 0) {
                    btnLogin.setBackgroundResource(R.drawable.custom_button_blue);
                } else {
                    btnLogin.setBackgroundResource(R.drawable.custom_button_grey);
                }
            }
        });
    }
}
