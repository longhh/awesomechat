package com.android.longhh.chat.data.firebase;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Users implements Serializable {
    private String id;
    private String username;
    private String imageURL;
    private String search;

    public Users(String id, String username, String imageURL, String search) {
        this.id = id;
        this.username = username;
        this.imageURL = imageURL;
        this.search = search;
    }

    public Users() {}


    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
