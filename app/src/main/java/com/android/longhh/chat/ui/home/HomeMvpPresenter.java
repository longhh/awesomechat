package com.android.longhh.chat.ui.home;

import com.android.longhh.chat.ui.base.MvpPresenter;

public interface HomeMvpPresenter<V extends HomeMvpView> extends MvpPresenter<V> {
    void readRooms();
    void readRequest();
}
