package com.android.longhh.chat.ui.home;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.ui.home.homemessenger.EventBusMess;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment implements HomeMvpView {

    public static final String TAG = HomeFragment.class.getSimpleName();

    @BindView(R.id.bottom_nav_home)
    BottomNavigationView bottomNavHome;

    @BindView(R.id.view_pager_home)
    ViewPager viewPagerHome;

    @Inject
    HomeMvpPresenter<HomeMvpView> mPresenter;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
    @Subscribe
    public void onEvent(EventBusMess event) {
        viewPagerHome.setCurrentItem(event.getMessage());
    }

    @Override
    protected void setUp(View view) {
        mPresenter.readRequest();
        mPresenter.readRooms();
        ViewPagerHomeAdapter adapter = new ViewPagerHomeAdapter(getActivity().getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPagerHome.setAdapter(adapter);

        viewPagerHome.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i) {
                    case 0:
                        bottomNavHome.getMenu().findItem(R.id.action_messenger).setChecked(true);
                        break;
                    case 1:
                        bottomNavHome.getMenu().findItem(R.id.action_friend).setChecked(true);
                        break;
                    case 2:
                        bottomNavHome.getMenu().findItem(R.id.action_profile).setChecked(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        bottomNavHome.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.action_messenger:
                    viewPagerHome.setCurrentItem(0);
                    break;
                case R.id.action_friend:
                    viewPagerHome.setCurrentItem(1);
                    break;
                case R.id.action_profile:
                    viewPagerHome.setCurrentItem(2);
                    break;
            }
            return true;
        });
    }

    @Override
    public void readRooms(Integer count) {
        if (bottomNavHome != null) {
            BadgeDrawable badgeDrawable = bottomNavHome.getOrCreateBadge(R.id.action_messenger);
            if(count > 0 ){
                badgeDrawable.setVisible(true);
                badgeDrawable.setNumber(count);
            }else {
                badgeDrawable.setVisible(false);
            }
        }
    }

    @Override
    public void readRequest(ArrayList<Users> users) {
        if (bottomNavHome != null) {
            BadgeDrawable badgeDrawable2 = bottomNavHome.getOrCreateBadge(R.id.action_friend);
            if(users.size() > 0 ){
                badgeDrawable2.setVisible(true);
                badgeDrawable2.setNumber(users.size());
            }else {
                badgeDrawable2.setVisible(false);
            }
        }
    }

}
