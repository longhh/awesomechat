package com.android.longhh.chat.ui.home.homeprofile.changeprofile;

import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.MvpView;

public interface ChangeProfileMvpView extends MvpView {

    void onSuccess(Users users);
}
