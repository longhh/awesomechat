package com.android.longhh.chat.utils.listener;

public interface OnItemClickListener {
    void sendRequestFriend(int position);
    void cancelRequestFriend(int position);
}
