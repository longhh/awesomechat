package com.android.longhh.chat.ui.register;


import androidx.annotation.NonNull;
import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class RegisterPresenter<V extends RegisterMvpView> extends BasePresenter<V>
        implements RegisterMvpPresenter<V> {

    @Inject
    public RegisterPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onRegister(String username, String email, String password) {
        getDataManager().createUser(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            assert getDataManager().getFirebaseUser() != null;
                            String userid = getDataManager().getFirebaseUser().getUid();

                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(AppConstants.FIRE_BASE_ID, userid);
                            hashMap.put(AppConstants.FIRE_BASE_USERNAME, username);
                            hashMap.put(AppConstants.FIRE_BASE_IMAGE_URL, AppConstants.FIRE_BASE_DEFAULT);
                            hashMap.put(AppConstants.FIRE_BASE_SEARCH, username.toLowerCase());

                            getDataManager().getDatabaseReferenceChild(AppConstants.FIRE_BASE_USERS, userid).setValue(hashMap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            getMvpView().successRegister();
                                        }
                                    });
                        } else {
                            getMvpView().fieldRegister();
                        }
                    }
                });
    }
}
