package com.android.longhh.chat.ui.register;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.android.longhh.chat.R;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.ui.home.HomeFragment;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterFragment extends BaseFragment implements RegisterMvpView {

    public static final String TAG = RegisterFragment.class.getSimpleName();

    @BindView(R.id.edt_register_name)
    MaterialEditText edtRegisterName;
    @BindView(R.id.edt_register_email)
    MaterialEditText edtRegisterEmail;
    @BindView(R.id.edt_register_password)
    MaterialEditText edtRegisterPassword;
    @BindView(R.id.checkbox_register)
    AppCompatCheckBox cbRegister;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.txt_back)
    TextView txtBack;
    @BindView(R.id.img_back)
    ImageView imgBack;

    @Inject
    RegisterMvpPresenter<RegisterMvpView> mPresenter;

    public static RegisterFragment newInstance() {
        Bundle args = new Bundle();
        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }

        return view;
    }

    @Override
    protected void setUp(View view) {
        changeColorButton();
        txtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    @OnClick({R.id.btn_register})
    public void onRegister() {
        String username = Objects.requireNonNull(edtRegisterName.getText()).toString();
        String email = Objects.requireNonNull(edtRegisterEmail.getText()).toString();
        String password = Objects.requireNonNull(edtRegisterPassword.getText()).toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || !cbRegister.isChecked()) {
            Toast.makeText(getActivity(), R.string.not_required, Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(getActivity(), R.string.fields_password, Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.onRegister(username, email, password);
        }
    }

    @Override
    public void successRegister() {
        Toast.makeText(getActivity(), R.string.register_success, Toast.LENGTH_SHORT).show();
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameContainer, HomeFragment.newInstance(), HomeFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void fieldRegister() {
        Toast.makeText(getActivity(), R.string.empty_details, Toast.LENGTH_SHORT).show();
    }


    private void changeColorButton() {
        edtRegisterName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Objects.requireNonNull(edtRegisterName.getText()).length() > 0
                        && Objects.requireNonNull(edtRegisterEmail.getText()).length() > 0
                        && Objects.requireNonNull(edtRegisterPassword.getText()).length() > 0
                        && cbRegister.isChecked()) {
                    btnRegister.setBackgroundResource(R.drawable.custom_button_blue);
                } else {
                    btnRegister.setBackgroundResource(R.drawable.custom_button_grey);
                }
            }
        });
        edtRegisterEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Objects.requireNonNull(edtRegisterName.getText()).length() > 0
                        && Objects.requireNonNull(edtRegisterEmail.getText()).length() > 0
                        && Objects.requireNonNull(edtRegisterPassword.getText()).length() > 0
                        && cbRegister.isChecked()) {
                    btnRegister.setBackgroundResource(R.drawable.custom_button_blue);
                } else {
                    btnRegister.setBackgroundResource(R.drawable.custom_button_grey);
                }
            }
        });
        edtRegisterPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Objects.requireNonNull(edtRegisterName.getText()).length() > 0
                        && Objects.requireNonNull(edtRegisterEmail.getText()).length() > 0
                        && Objects.requireNonNull(edtRegisterPassword.getText()).length() > 0
                        && cbRegister.isChecked()) {
                    btnRegister.setBackgroundResource(R.drawable.custom_button_blue);
                } else {
                    btnRegister.setBackgroundResource(R.drawable.custom_button_grey);
                }
            }
        });
        cbRegister.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Objects.requireNonNull(edtRegisterName.getText()).length() > 0
                        && Objects.requireNonNull(edtRegisterEmail.getText()).length() > 0
                        && Objects.requireNonNull(edtRegisterPassword.getText()).length() > 0
                        && cbRegister.isChecked()) {
                    btnRegister.setBackgroundResource(R.drawable.custom_button_blue);
                } else {
                    btnRegister.setBackgroundResource(R.drawable.custom_button_grey);
                }
            }
        });
    }
}
