package com.android.longhh.chat.ui.home.homemessenger;

public class EventBusMess {
    int message;

    public EventBusMess(int message) {
        this.message = message;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }
}
