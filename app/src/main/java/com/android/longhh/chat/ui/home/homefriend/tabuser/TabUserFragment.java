package com.android.longhh.chat.ui.home.homefriend.tabuser;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.android.longhh.chat.R;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.ui.home.homefriend.HomeFriendFragment;
import com.android.longhh.chat.utils.listener.OnItemClickListener;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TabUserFragment extends BaseFragment implements TabUserMvpView {

    @Inject
    TabUserMvpPresenter<TabUserMvpView> mPresenter;

    @BindView(R.id.recycler_view_tab_user)
    RecyclerView recyclerViewTabUser;

    private TabUserAdapter adapter;

    public static TabUserFragment newInstance(Bundle args) {
        TabUserFragment fragment = new TabUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static TabUserFragment newInstance() {
        Bundle args = new Bundle();
        TabUserFragment fragment = new TabUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_user, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        initAdapter();
        HomeFriendFragment homeFriendFragment = (HomeFriendFragment) getParentFragment();
        if (homeFriendFragment != null) {
            homeFriendFragment.setOnTextChanged2(new HomeFriendFragment.OnTextChanged2() {
                @Override
                public void onTextChanged2(String word) {
                    if (word != null) {
                        mPresenter.searchUser(word);
                    }
                }
            });
        }
        recyclerViewTabUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }

    private void initAdapter() {
        recyclerViewTabUser.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new TabUserAdapter(getContext(), new ArrayList<>());
        recyclerViewTabUser.setAdapter(adapter);
    }

    @Override
    public void onSuccess(List<UserSort> userArrayList) {
        if (adapter != null) {
            adapter.setData(userArrayList);

            adapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void sendRequestFriend(int position) {
                    mPresenter.sendRequest(userArrayList.get(position).getUsers());
                }

                @Override
                public void cancelRequestFriend(int position) {
                    mPresenter.cancelRequest(userArrayList.get(position).getUsers());
                }
            });
        }
    }
}