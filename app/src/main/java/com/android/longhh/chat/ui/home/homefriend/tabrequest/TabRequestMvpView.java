package com.android.longhh.chat.ui.home.homefriend.tabrequest;

import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface TabRequestMvpView extends MvpView {
    void readSender(ArrayList<Users> users);
    void readReceiver(ArrayList<Users> users);
}
