package com.android.longhh.chat.data.firebase;

public class Message {
    private String senderId;
    private String content;
    private String time;

    public Message() {
    }

    public Message(String senderId, String content, String time) {
        this.senderId = senderId;
        this.content = content;
        this.time = time;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
