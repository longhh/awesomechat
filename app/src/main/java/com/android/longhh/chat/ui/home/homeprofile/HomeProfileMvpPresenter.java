package com.android.longhh.chat.ui.home.homeprofile;

import com.android.longhh.chat.ui.base.MvpPresenter;

public interface HomeProfileMvpPresenter <V extends HomeProfileMvpView> extends MvpPresenter<V> {

    void showProfile();

    void signOut();
}

