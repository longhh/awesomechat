package com.android.longhh.chat.ui.home.homefriend.tabrequest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.utils.AppConstants;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SenderAdapter extends RecyclerView.Adapter<SenderAdapter.SenderViewHolder>{

    private final Context mContext;
    private ArrayList<Users> data;

    public SenderAdapter(Context mContext, ArrayList<Users> data) {
        this.mContext = mContext;
        this.data = data;
    }
    public interface IOnClickItemSender{
        void itemClick(Users users);
    }
    public IOnClickItemSender iOnClickItem;

    public void setOnClickItemSender(IOnClickItemSender iOnClickItem) {
        this.iOnClickItem = iOnClickItem;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(ArrayList<Users> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SenderAdapter.SenderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_tab_request_cancel, viewGroup, false);
        return new SenderAdapter.SenderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SenderAdapter.SenderViewHolder receiverViewHolder, int i) {
        Users users = data.get(i);
        receiverViewHolder.bindView(users, iOnClickItem);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    class SenderViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_circle_friend)
        CircleImageView imgCircleFriend;

        @BindView(R.id.txt_username)
        AppCompatTextView txtUserName;

        @BindView(R.id.btn_cancel_request)
        AppCompatButton btnCancelFriend;

        public SenderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        public void bindView(Users users, IOnClickItemSender iOnClickItem){
            Typeface typeBold = ResourcesCompat.getFont(mContext, R.font.lato_bold);
            txtUserName.setTypeface(typeBold);
            txtUserName.setText(users.getUsername());
            if (users.getImageURL().equals(AppConstants.FIRE_BASE_DEFAULT)) {
                Glide.with(mContext).load(R.drawable.ic_avt).into(imgCircleFriend);
            } else {
                Glide.with(mContext).load(users.getImageURL()).into(imgCircleFriend);
            }
            btnCancelFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (iOnClickItem != null){
                        iOnClickItem.itemClick(users);
                    }
                }
            });
        }
    }
}