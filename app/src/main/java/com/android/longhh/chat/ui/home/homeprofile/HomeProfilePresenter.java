package com.android.longhh.chat.ui.home.homeprofile;

import androidx.annotation.NonNull;
import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;

public class HomeProfilePresenter<V extends HomeProfileMvpView> extends BasePresenter<V>
        implements HomeProfileMvpPresenter<V> {


    @Inject
    public HomeProfilePresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void showProfile() {
        String email = getDataManager().getFirebaseUser().getEmail();
        getDataManager().getDatabaseReferenceChild(AppConstants.FIRE_BASE_USERS, getDataManager().getFirebaseUser().getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        getMvpView().onSuccess(users, email);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void signOut() {
        getDataManager().getFirebaseAuth().signOut();
    }

}