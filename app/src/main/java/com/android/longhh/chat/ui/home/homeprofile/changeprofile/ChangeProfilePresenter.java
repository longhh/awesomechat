package com.android.longhh.chat.ui.home.homeprofile.changeprofile;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.util.HashMap;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;

public class ChangeProfilePresenter<V extends ChangeProfileMvpView> extends BasePresenter<V>
        implements ChangeProfileMvpPresenter<V> {

    @Inject
    public ChangeProfilePresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void setUpView() {
        getDataManager().getDatabaseReferenceChild(AppConstants.FIRE_BASE_USERS, getDataManager().getFirebaseUser().getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        getMvpView().onSuccess(users);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void uploadImage(Uri mUri) {
        StorageReference reference = FirebaseStorage.getInstance().getReference();
        StorageReference riversRef = reference.child("images/" + mUri.getLastPathSegment());
        riversRef.putFile(mUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        getDataManager().getDatabaseReference(AppConstants.REFERENCE_USERS)
                                .child(getDataManager().getFirebaseUserId())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        Users users = dataSnapshot.getValue(Users.class);
                                        if ((users != null)) {
                                            users.setImageURL(uri.toString());
                                            dataSnapshot.getRef().setValue(users);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                    }
                });
            }
        });
    }

    @Override
    public void uploadName(String name) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(AppConstants.FIRE_BASE_USERNAME, name);
        map.put(AppConstants.FIRE_BASE_SEARCH, name.toLowerCase());
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_USERS, getDataManager().getFirebaseUserId()).updateChildren(map);
    }
}
