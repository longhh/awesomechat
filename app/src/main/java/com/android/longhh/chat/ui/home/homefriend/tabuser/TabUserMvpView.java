package com.android.longhh.chat.ui.home.homefriend.tabuser;

import com.android.longhh.chat.ui.base.MvpView;
import java.util.List;

public interface TabUserMvpView extends MvpView {
    void onSuccess(List<UserSort> userArrayList);
}