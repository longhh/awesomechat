package com.android.longhh.chat.ui.home.messenger;


import android.net.Uri;
import androidx.annotation.NonNull;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Message;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.fcm.Client;
import com.android.longhh.chat.fcm.Data;
import com.android.longhh.chat.fcm.MyResponse;
import com.android.longhh.chat.fcm.Sender;
import com.android.longhh.chat.fcm.Token;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.File;
import java.util.ArrayList;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessengerPresenter<V extends MessengerMvpView> extends BasePresenter<V>
        implements MessengerMvpPresenter<V> {

    private Users sender;
    private Room room;
    APIService apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

    @Inject
    public MessengerPresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void setRoom(Room room) {
        this.room = room;
        getDataSender();
        getDataChatRoom();
    }

    private void getDataSender() {
        getDataManager().getDatabaseReferenceChild(AppConstants.FIRE_BASE_USERS, getDataManager().getFirebaseUser().getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        sender = dataSnapshot.getValue(Users.class);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }

    private void getDataChatRoom() {
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_MESSAGES, room.getId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<Message> messages = new ArrayList<>();

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Message message = snapshot.getValue(Message.class);
                            if (message != null) {
                                messages.add(message);
                            }
                        }
                        getMvpView().onSuccess(messages);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
        // Reset unread message
        getDataManager().updateUnreadMessage(room.getId())
                .addOnSuccessListener(unused -> room.setUnreadMessage(0));
    }

    @Override
    public void sendMessage(String message) {
        Message newMessage = new Message(sender.getId(), message, getDataManager().getCurrentTime());
        getDataManager().getDatabaseReference()
                .child(AppConstants.REFERENCE_MESSAGES)
                .child(room.getId())
                .push()
                .setValue(newMessage)
                .addOnSuccessListener(unused -> {
                    updateChatRoom(message);
                })
                .addOnFailureListener(e -> getMvpView().showMessage(e.getMessage()));
        sendNotification(message);
    }

    private void sendNotification(String message) {
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_TOKENS)
                .orderByKey().equalTo(room.getReceiver())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Token token = dataSnapshot.getValue(Token.class);
                            Data data = new Data(getDataManager().getFirebaseUserId(), R.mipmap.ic_launcher,
                                    sender.getUsername() + " : " + message, getMvpView().getContext().getString(R.string.new_message), room.getReceiver());
                            assert token != null;
                            Sender sender = new Sender(data, token.getToken());
                            apiService.sendNotifications(sender)
                                    .enqueue(new Callback<MyResponse>() {
                                        @Override
                                        public void onResponse(@NonNull Call<MyResponse> call, @NonNull Response<MyResponse> response) {
                                            assert response.code() != 200 || response.body() != null;
                                        }
                                        @Override
                                        public void onFailure(@NonNull Call<MyResponse> call, @NonNull Throwable t) {

                                        }
                                    });
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    @Override
    public void sendImage(ArrayList<String> listImg) {
        StorageReference reference = FirebaseStorage.getInstance().getReference();
        for (int i = 0; i < listImg.size(); i++) {
            Uri file = Uri.fromFile(new File(listImg.get(i)));
            StorageReference riversRef = reference.child("images/" + file.getLastPathSegment());
            riversRef.putFile(file).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(@NonNull Uri uri) {
                            Message newMessage = new Message(sender.getId(), uri.toString(), getDataManager().getCurrentTime());
                            getDataManager().getDatabaseReference()
                                    .child(AppConstants.REFERENCE_MESSAGES)
                                    .child(room.getId())
                                    .push()
                                    .setValue(newMessage)
                                    .addOnSuccessListener(unused -> {
                                        String receiver = sender.getUsername() + getMvpView().getContext().getString(R.string.send_image);
                                        updateChatRoom(getMvpView().getContext().getString(R.string.you_send_image), receiver);
                                    })
                                    .addOnFailureListener(e -> getMvpView().showMessage(e.getMessage()));
                        }
                    });
                }
            });
        }
        sendNotification(getMvpView().getContext().getString(R.string.send_image));
    }

    @Override
    public void sendSticker(String sticker) {
        Message newMessage = new Message(sender.getId(), sticker, getDataManager().getCurrentTime());
        getDataManager().getDatabaseReference()
                .child(AppConstants.REFERENCE_MESSAGES)
                .child(room.getId())
                .push()
                .setValue(newMessage)
                .addOnSuccessListener(unused -> {
                    String receiver = sender.getUsername() + " " + getMvpView().getContext().getString(R.string.send_sticker);
                    updateChatRoom(getMvpView().getContext().getString(R.string.you_send_sticker), receiver);
                })
                .addOnFailureListener(e -> getMvpView().showMessage(e.getMessage()));
        sendNotification(getMvpView().getContext().getString(R.string.send_sticker));
    }

    private void updateChatRoom(String message) {
        room.setLastMessage(message);
        room.setTimestamp(getDataManager().getTimestamp());
        //Update chat room sender
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_ROOMS, getDataManager().getFirebaseUser().getUid())
                .child(room.getId())
                .setValue(room);

        //Update chat room receiver
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_ROOMS, room.getReceiver())
                .child(room.getId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Room room = dataSnapshot.getValue(Room.class);
                        if (room != null) {
                            room.setLastMessage(message);
                            int unread = room.getUnreadMessage() + 1;
                            room.setName(sender.getUsername());
                            room.setThumbnail(sender.getImageURL());
                            room.setSearchByName(sender.getUsername().toLowerCase());
                            room.setUnreadMessage(unread);
                            room.setTimestamp(getDataManager().getTimestamp());
                            dataSnapshot.getRef().setValue(room);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }

    private void updateChatRoom(String sender, String receiver) {
        room.setLastMessage(sender);
        room.setTimestamp(getDataManager().getTimestamp());

        //Update chat room sender
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_ROOMS, getDataManager().getFirebaseUser().getUid())
                .child(room.getId())
                .setValue(room);

        //Update chat room receiver
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_ROOMS, room.getReceiver())
                .child(room.getId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Room room = dataSnapshot.getValue(Room.class);
                        if (room != null) {
                            room.setLastMessage(receiver);
                            int unread = room.getUnreadMessage() + 1;
                            room.setUnreadMessage(unread);
                            room.setTimestamp(getDataManager().getTimestamp());
                            dataSnapshot.getRef().setValue(room);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }

}
