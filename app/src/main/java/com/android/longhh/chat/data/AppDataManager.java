/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.longhh.chat.data;


import android.content.Context;

import com.android.longhh.chat.data.db.DbHelper;
import com.android.longhh.chat.data.db.model.Option;
import com.android.longhh.chat.data.db.model.Question;
import com.android.longhh.chat.data.db.model.User;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.data.network.ApiHeader;
import com.android.longhh.chat.data.network.ApiHelper;
import com.android.longhh.chat.data.network.model.BlogResponse;
import com.android.longhh.chat.data.network.model.LoginRequest;
import com.android.longhh.chat.data.network.model.LoginResponse;
import com.android.longhh.chat.data.network.model.LogoutResponse;
import com.android.longhh.chat.data.network.model.OpenSourceResponse;
import com.android.longhh.chat.data.prefs.PreferencesHelper;
import com.android.longhh.chat.di.ApplicationContext;
import com.android.longhh.chat.utils.AppConstants;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mDbHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public Observable<Long> insertUser(User user) {
        return mDbHelper.insertUser(user);
    }

    @Override
    public Observable<List<User>> getAllUsers() {
        return mDbHelper.getAllUsers();
    }


    @Override
    public Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest
                                                              request) {
        return mApiHelper.doServerLoginApiCall(request);
    }

    @Override
    public Single<LogoutResponse> doLogoutApiCall() {
        return mApiHelper.doLogoutApiCall();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public Long getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }

    @Override
    public void updateApiHeader(Long userId, String accessToken) {
        mApiHelper.getApiHeader().getProtectedApiHeader().setUserId(userId);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public void updateUserInfo(
            String accessToken,
            Long userId,
            LoggedInMode loggedInMode,
            String userName,
            String email,
            String profilePicPath) {

        setAccessToken(accessToken);
        setCurrentUserId(userId);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentUserName(userName);
        setCurrentUserEmail(email);
        setCurrentUserProfilePicUrl(profilePicPath);

        updateApiHeader(userId, accessToken);
    }

    @Override
    public void setDataUser(Users user) {
        mPreferencesHelper.setDataUser(user);
    }

    @Override
    public Users getDataUser() {
        return mPreferencesHelper.getDataUser();
    }

    @Override
    public DatabaseReference getDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public DatabaseReference getDatabaseReference(String path) {
        return FirebaseDatabase.getInstance().getReference(path);
    }

    @Override
    public FirebaseAuth getFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    @Override
    public FirebaseUser getFirebaseUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public String getFirebaseUserId() {
        return getFirebaseUser().getUid();
    }

    @Override
    public DatabaseReference getDatabaseReferenceChild(String table, String table1) {
        return FirebaseDatabase.getInstance().getReference(table).child(table1);
    }

    @Override
    public Task<AuthResult> createUser(String email, String password) {
        return FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password);
    }

    @Override
    public Task<AuthResult> signIn(String email, String password) {
        return FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password);
    }

    @Override
    public StorageReference getStorageReference(String table) {
        return FirebaseStorage.getInstance().getReference(table);
    }

    @Override
    public String getCurrentTime() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(date);
        return formattedDate;
    }

    @Override
    public Task<DataSnapshot> getListRoomByUserOnceTime() {
        return getDatabaseReference(AppConstants.REFERENCE_ROOMS)
                .child(getFirebaseUserId())
                .get();
    }

    @Override
    public String generateRoomKey() {
        return getDatabaseReference(AppConstants.REFERENCE_ROOMS)
                .child(getFirebaseUserId())
                .push()
                .getKey();
    }

    @Override
    public Task<Void> createChatRoom(Users receiver, Room newRoom) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(getFirebaseUserId() + "/" + newRoom.getId(), newRoom.toMap());
        childUpdates.put(receiver.getId() + "/" + newRoom.getId(), createReceiverRoom(newRoom).toMap());
        return getDatabaseReference(AppConstants.REFERENCE_ROOMS)
                .updateChildren(childUpdates);
    }

    private Room createReceiverRoom(Room newRoom) {
        Users currentUser = getDataUser();
        return new Room(newRoom.getId(), currentUser.getUsername(), currentUser.getImageURL(),
                currentUser.getId(), newRoom.getUnreadMessage(), newRoom.getLastMessage(), newRoom.getTimestamp(), newRoom.getSearchByName());
    }

    @Override
    public DatabaseReference getCurrentDataUser() {
        return getDatabaseReference(AppConstants.REFERENCE_USERS).child(getFirebaseUserId());
    }

    @Override
    public Task<Void> updateUnreadMessage(String roomId) {
        return getDatabaseReference(AppConstants.REFERENCE_ROOMS)
                .child(getFirebaseUserId())
                .child(roomId)
                .child(AppConstants.UNREAD_MESSAGE)
                .setValue(0);

    }

    @Override
    public DatabaseReference getChatRoomMessage(String receiverId) {
        return getDatabaseReference(AppConstants.REFERENCE_MESSAGES).child(receiverId);
    }

    @Override
    public String getTimestamp() {
        Long tsLong = System.currentTimeMillis();
        String ts = tsLong.toString();
        return ts;
    }

    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(
                null,
                null,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null);
    }

    @Override
    public Observable<Boolean> isQuestionEmpty() {
        return mDbHelper.isQuestionEmpty();
    }

    @Override
    public Observable<Boolean> isOptionEmpty() {
        return mDbHelper.isOptionEmpty();
    }

    @Override
    public Observable<Boolean> saveQuestion(Question question) {
        return mDbHelper.saveQuestion(question);
    }

    @Override
    public Observable<Boolean> saveOption(Option option) {
        return mDbHelper.saveOption(option);
    }

    @Override
    public Observable<Boolean> saveQuestionList(List<Question> questionList) {
        return mDbHelper.saveQuestionList(questionList);
    }

    @Override
    public Observable<Boolean> saveOptionList(List<Option> optionList) {
        return mDbHelper.saveOptionList(optionList);
    }

    @Override
    public Observable<List<Question>> getAllQuestions() {
        return mDbHelper.getAllQuestions();
    }

    @Override
    public Single<BlogResponse> getBlogApiCall() {
        return mApiHelper.getBlogApiCall();
    }

    @Override
    public Single<OpenSourceResponse> getOpenSourceApiCall() {
        return mApiHelper.getOpenSourceApiCall();
    }

}
