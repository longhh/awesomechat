package com.android.longhh.chat.ui.home.homefriend.tabfriend;

import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Comparator;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;

public class TabFriendPresenter<V extends TabFriendMvpView> extends BasePresenter<V>
        implements TabFriendMvpPresenter<V> {

    ArrayList<Users> userArrayList = new ArrayList<>();
    ArrayList<Users> users = new ArrayList<>();
    ArrayList<SortUser> sortUsers = new ArrayList<>();

    @Inject
    public TabFriendPresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void readUser() {
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_FRIENDS)
                .child(getDataManager().getFirebaseUserId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userArrayList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && getDataManager().getFirebaseUser() != null) {
                                if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                    userArrayList.add(user);
                                }
                            }
                        }
                        readUserFriend(userArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void readUserFriend(ArrayList<Users> userArrayList) {
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_USERS)
                .addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        users.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            for (int i = 0; i < userArrayList.size(); i++) {
                                if (user != null && user.getId().equals(userArrayList.get(i).getId())) {
                                    users.add(user);
                                }
                            }
                        }
                        sortListUserWithAlphabet(users);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void sortListUserWithAlphabet(ArrayList<Users> users) {
        users.sort(Comparator.comparing(user -> String.valueOf(user.getUsername().charAt(0)).toUpperCase()));
        filterListUser();
    }

    private void filterListUser() {
        sortUsers = new ArrayList<>();
        String lastHeader = "";
        for (Users user : users) {
            String header = String.valueOf(user.getUsername().charAt(0)).toUpperCase();
            if (!header.equals(lastHeader)) {
                lastHeader = header;
                sortUsers.add(new SortUser(header, null, true));
            }
            sortUsers.add(new SortUser(header, user, false));
        }
        getMvpView().onSuccess(sortUsers);
    }

    @Override
    public void checkExistsRoom(Users receiver) {
        getDataManager().getListRoomByUserOnceTime()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        for (DataSnapshot snapshot : task.getResult().getChildren()) {
                            Room room = snapshot.getValue(Room.class);
                            if (room != null && room.getReceiver() != null
                                    && room.getReceiver().equals(receiver.getId())) {
                                getMvpView().openChatRoom(room);
                                return;
                            }
                        }
                        createChatRoom(receiver);
                    } else if (task.getException() != null) {
                        getMvpView().showMessage(task.getException().getMessage());
                    }
                });
    }

    private void createChatRoom(Users receiver) {
        String roomKey = getDataManager().generateRoomKey();
        Room newRoom = new Room(roomKey, receiver.getUsername(),
                receiver.getImageURL(), receiver.getId(), 0, "", getDataManager().getTimestamp(), receiver.getUsername().toLowerCase());
        getDataManager().createChatRoom(receiver, newRoom)
                .addOnSuccessListener(unused -> {
                    getMvpView().openChatRoom(newRoom);
                })
                .addOnFailureListener(e -> {
                    getMvpView().showMessage(e.getMessage());
                });

    }

    @Override
    public void searchUser(String word) {
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_FRIENDS)
                .child(getDataManager().getFirebaseUserId())
                .addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        userArrayList.clear();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Users user = dataSnapshot.getValue(Users.class);
                            assert user != null;
                            if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                userArrayList.add(user);
                            }
                        }
                        readUserSearch(userArrayList, word);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void readUserSearch(ArrayList<Users> userArrayList, String word) {
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_USERS)
                .orderByChild(AppConstants.FIRE_BASE_SEARCH)
                .startAt(word).endAt(word + "\uf8ff")
                .addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        users.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            for (int i = 0; i < userArrayList.size(); i++) {
                                if (user != null && user.getId().equals(userArrayList.get(i).getId())) {
                                    users.add(user);
                                }
                            }
                        }
                        sortListUser(users);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void sortListUser(ArrayList<Users> userArrayList) {
        userArrayList.sort(Comparator.comparing(user -> String.valueOf(user.getUsername().charAt(0)).toUpperCase()));
        filterListUserSearch(userArrayList);
    }

    private void filterListUserSearch(ArrayList<Users> userArrayList) {
        ArrayList<SortUser> usersSortSearch = new ArrayList<>();
        String lastHeader = "";
        for (Users user : userArrayList) {
            String header = String.valueOf(user.getUsername().charAt(0)).toUpperCase();
            if (!header.equals(lastHeader)) {
                lastHeader = header;
                usersSortSearch.add(new SortUser(header, null, true));
            }
            usersSortSearch.add(new SortUser(header, user, false));
        }
        getMvpView().onSuccess(usersSortSearch);
    }
}