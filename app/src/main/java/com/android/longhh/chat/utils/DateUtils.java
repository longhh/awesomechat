package com.android.longhh.chat.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private Date date;
    private long longTime;
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat full = new SimpleDateFormat(AppConstants.TimeFormat.FULL);
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat monthFormat = new SimpleDateFormat(AppConstants.TimeFormat.MONTH);
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat dayFormat = new SimpleDateFormat(AppConstants.TimeFormat.DAY);
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat hourFormat = new SimpleDateFormat(AppConstants.TimeFormat.HOURS);
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat dateFormat = new SimpleDateFormat(AppConstants.TimeFormat.DATE);

    public DateUtils(Date date) {
        this.date = date;
    }

    public DateUtils(long longTime) {
        this.longTime = longTime;
    }

    public String getMonth(){
        return monthFormat.format(date);
    }
    public String getDate(){
        return dateFormat.format(date);
    }
    public String getDay(){
        return dayFormat.format(date);
    }
    public String getHour(){
        return hourFormat.format(date);
    }

    public String getMonthLong(){
        return monthFormat.format(longTime);
    }
    public String getDateLong(){
        return dateFormat.format(longTime);
    }
    public String getDayLong(){
        return dayFormat.format(longTime);
    }
    public String getHourLong(){
        return hourFormat.format(longTime);
    }
}
