package com.android.longhh.chat.ui.home;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.android.longhh.chat.ui.home.homefriend.HomeFriendFragment;
import com.android.longhh.chat.ui.home.homemessenger.HomeMessengerFragment;
import com.android.longhh.chat.ui.home.homeprofile.HomeProfileFragment;


public class ViewPagerHomeAdapter extends FragmentStatePagerAdapter {


    public ViewPagerHomeAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 1:
                return new HomeFriendFragment();
            case 2:
                return new HomeProfileFragment();
            default:
                return new HomeMessengerFragment();
        }
    }
    @Override
    public int getCount() {
        return 3;
    }
}
