package com.android.longhh.chat.ui.home.homefriend.tabuser;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.ui.home.homefriend.tabfriend.SortUser;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class TabUserPresenter <V extends TabUserMvpView> extends BasePresenter<V>
        implements TabUserMvpPresenter<V> {

    @Inject
    public TabUserPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    Users sender;

    private List<Users> friends = new ArrayList<>();
    private List<Users> friendRequestSends = new ArrayList<>();
    private ArrayList<Users> users = new ArrayList<>();
    private List<UserSort> userSorts = new ArrayList<>();

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        getListFriend();
        getDataSender();
    }

    private void getListFriend() {
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_FRIENDS, getDataManager().getFirebaseUserId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        friends = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null) {
                                friends.add(user);
                            }
                        }
                        getListFriendRequestSend();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().onError(databaseError.getMessage());
                    }
                });
    }

    private void getListFriendRequestSend() {
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_FRIEND_REQUEST, getDataManager().getFirebaseUserId())
                .child(AppConstants.REFERENCE_REQUEST_SEND)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        friendRequestSends = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null) {
                                friendRequestSends.add(user);
                            }
                        }
                        getListUser();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getListUser() {
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_USERS)
                .addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        users = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && !user.getId().equals(getDataManager().getFirebaseUserId())) {
                                users.add(user);
                            }
                        }
                        sortListUserWithAlphabet();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().onError(databaseError.getMessage());
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void sortListUserWithAlphabet() {
        users.sort(Comparator.comparing(user -> String.valueOf(user.getUsername().charAt(0)).toUpperCase()));
        filterListUser();
    }

    private void filterListUser() {
        userSorts = new ArrayList<>();
        String lastHeader = "";
        for (Users user : users) {
            String header = String.valueOf(user.getUsername().charAt(0)).toUpperCase();
            AppConstants.StateUser currentState = getStateUser(user);
            if (!header.equals(lastHeader)) {
                lastHeader = header;
                userSorts.add(new UserSort(header, null, true, currentState));
            }
            userSorts.add(new UserSort(header, user, false, currentState));
        }
        getMvpView().onSuccess(userSorts);
    }

    private AppConstants.StateUser getStateUser(Users user) {
        AppConstants.StateUser state = AppConstants.StateUser.OTHERS;
        for (Users friend : friends) {
            if (user.getId().equals(friend.getId())) {
                state = AppConstants.StateUser.FRIEND;
            }
        }
        for (Users friendRequest : friendRequestSends) {
            if (user.getId().equals(friendRequest.getId())) {
                state = AppConstants.StateUser.REQUEST_FRIEND;
            }
        }
        return state;
    }

    private void getDataSender() {
        getDataManager().getCurrentDataUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        sender = dataSnapshot.getValue(Users.class);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }

    private void removeValueSender(Users users, String id, String table) {
        Query query = getDataManager().getDatabaseReference()
                .child(AppConstants.REFERENCE_FRIEND_REQUEST)
                .child(id)
                .child(table)
                .orderByChild(AppConstants.FIRE_BASE_ID).equalTo(users.getId());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void sendRequest(Users user) {
        getDataManager().getDatabaseReference()
                .child(AppConstants.REFERENCE_FRIEND_REQUEST)
                .child(getDataManager().getFirebaseUserId())
                .child(AppConstants.REFERENCE_REQUEST_SEND)
                .push()
                .setValue(user);
        getDataManager().getDatabaseReference()
                .child(AppConstants.REFERENCE_FRIEND_REQUEST)
                .child(user.getId())
                .child(AppConstants.REFERENCE_REQUEST_RECEIVE)
                .push()
                .setValue(sender);
    }

    @Override
    public void cancelRequest(Users user) {
        removeValueSender(user, getDataManager().getFirebaseUserId(), AppConstants.REFERENCE_REQUEST_SEND);
        removeValueSender(sender, user.getId(), AppConstants.REFERENCE_REQUEST_RECEIVE);
    }

    @Override
    public void searchUser(String word) {
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_USERS)
                .orderByChild(AppConstants.FIRE_BASE_SEARCH)
                .startAt(word).endAt(word + "\uf8ff")
                .addValueEventListener(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        users.clear();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Users user = dataSnapshot.getValue(Users.class);
                            assert user != null;
                            if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                users.add(user);
                            }
                        }
                        sortListUserWithAlphabet();
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
}
