package com.android.longhh.chat.ui.home.homeprofile.changeprofile;

import android.net.Uri;

import com.android.longhh.chat.ui.base.MvpPresenter;

public interface ChangeProfileMvpPresenter <V extends ChangeProfileMvpView> extends MvpPresenter<V> {

    void setUpView();

    void uploadImage(Uri mUri);

    void uploadName(String name);
}
