package com.android.longhh.chat.ui.home.homemessenger;

import com.android.longhh.chat.ui.base.MvpPresenter;

public interface HomeMessengerMvpPresenter<V extends HomeMessengerMvpView> extends MvpPresenter<V> {
    void searchByName(String name);
}
