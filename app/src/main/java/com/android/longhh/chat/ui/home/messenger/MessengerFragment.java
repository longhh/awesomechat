package com.android.longhh.chat.ui.home.messenger;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Message;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.utils.AppConstants;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessengerFragment extends BaseFragment implements MessengerMvpView {

    public static final String TAG = MessengerFragment.class.getSimpleName();

    public static final int MY_READ_PERMISSION_CODE = 1;

    @BindView(R.id.img_circle_messenger)
    CircleImageView imgCircleMessenger;
    @BindView(R.id.txt_username_messenger)
    TextView txtUsernameMessenger;
    @BindView(R.id.img_back_messenger)
    ImageView imgBackMessenger;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.edt_send)
    EditText edtSend;
    @BindView(R.id.recycler_view_messenger)
    RecyclerView recyclerViewMessenger;
    @BindView(R.id.select_image)
    CircleImageView selectImage;
    @BindView(R.id.layout_gallery)
    ConstraintLayout layoutGallery;
    @BindView(R.id.recycler_view_gallery)
    RecyclerView recyclerViewGallery;
    @BindView(R.id.layout_sticker)
    ConstraintLayout layoutSticker;
    @BindView(R.id.recycler_view_sticker)
    RecyclerView recyclerViewSticker;
    @BindView(R.id.img_sticker)
    ImageView imgSticker;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.txt_time_chat)
    TextView txtTimeChat;

    MessengerAdapter messengerAdapter;
    SelectImageAdapter selectImageAdapter;
    StickerAdapter stickerAdapter;
    boolean isCheckedGallery = false;
    boolean isCheckedSticker = false;

    @Inject
    MessengerMvpPresenter<MessengerMvpView> mPresenter;

    public static MessengerFragment newInstance() {
        Bundle args = new Bundle();
        MessengerFragment fragment = new MessengerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static MessengerFragment newInstance(Bundle args) {
        MessengerFragment fragment = new MessengerFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messenger, container, false);
        hideKeyboard();
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    @Override
    protected void setUp(View view) {
        initData();
        initAdapter();

        imgBackMessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backHomeFriendFragment();
                hideKeyboard();
            }
        });
        imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = edtSend.getText().toString();
                if (!msg.equals("")) {
                    mPresenter.sendMessage(msg);
                } else {
                    Toast.makeText(getActivity(), R.string.cant_send, Toast.LENGTH_SHORT).show();
                }
                edtSend.setText("");
            }
        });

        selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isCheckedGallery) {
                    selectImage.setImageResource(R.drawable.ic_add_photo);
                    imgSticker.setImageResource(R.drawable.ic_smile_gray);
                    layoutGallery.setVisibility(View.VISIBLE);
                    layoutSticker.setVisibility(View.GONE);
                    requestPermission();
                    hideKeyboard();
                    isCheckedGallery = true;
                    isCheckedSticker = false;
                } else {
                    layoutGallery.setVisibility(View.GONE);
                    selectImage.setImageResource(R.drawable.ic_add_photo_gray);
                    isCheckedGallery = false;
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectImageAdapter.getListImg() != null) {
                    mPresenter.sendImage(selectImageAdapter.getListImg());
                }
                selectImageAdapter.clearList();
                btnCancel.setVisibility(View.GONE);
                btnSend.setVisibility(View.GONE);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnCancel.setVisibility(View.GONE);
                btnSend.setVisibility(View.GONE);
                selectImageAdapter.clearList();
            }
        });

        imgSticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCheckedSticker) {
                    imgSticker.setImageResource(R.drawable.ic_smile);
                    selectImage.setImageResource(R.drawable.ic_add_photo_gray);
                    layoutSticker.setVisibility(View.VISIBLE);
                    layoutGallery.setVisibility(View.GONE);
                    loadSticker();
                    hideKeyboard();
                    isCheckedSticker = true;
                    isCheckedGallery = false;
                } else {
                    imgSticker.setImageResource(R.drawable.ic_smile_gray);
                    layoutSticker.setVisibility(View.GONE);
                    isCheckedSticker = false;
                }
            }
        });

        edtSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage.setImageResource(R.drawable.ic_add_photo_gray);
                imgSticker.setImageResource(R.drawable.ic_smile_gray);
                layoutSticker.setVisibility(View.GONE);
                layoutGallery.setVisibility(View.GONE);
                isCheckedSticker = false;
                isCheckedGallery = false;
            }
        });

        recyclerViewMessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }

    private void initData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.BUNDLE_KEY_ROOM)) {
            Room room = (Room) getArguments().getSerializable(AppConstants.BUNDLE_KEY_ROOM);
            if (room != null) {
                mPresenter.setRoom(room);
                txtUsernameMessenger.setText(room.getName());
                if (getContext() != null) {
                    Glide.with(getContext())
                            .load(room.getThumbnail().equals(AppConstants.FIRE_BASE_DEFAULT)
                                    ? R.drawable.ic_avt : room.getThumbnail())
                            .into(imgCircleMessenger);
                }
            }
        }
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerViewMessenger.setLayoutManager(linearLayoutManager);

        recyclerViewMessenger.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    RecyclerView.LayoutManager lm = recyclerViewMessenger.getLayoutManager();
                    if (lm != null && lm instanceof LinearLayoutManager) {
                        int currPosition = ((LinearLayoutManager) lm).findFirstVisibleItemPosition();
                        String time = null;
                        try {
                            time = messengerAdapter.getTime(currPosition);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        txtTimeChat.setVisibility(View.VISIBLE);
                        txtTimeChat.setText(time);
                    }
                }
            }
        });

        if (getArguments() != null && getArguments().containsKey(AppConstants.BUNDLE_KEY_ROOM)) {
            Room room = (Room) getArguments().getSerializable(AppConstants.BUNDLE_KEY_ROOM);
            if (room != null) {
                messengerAdapter = new MessengerAdapter(getContext(), new ArrayList<>(), room.getThumbnail());
                recyclerViewMessenger.setAdapter(messengerAdapter);
            }
        }
    }

    @Override
    public void onSuccess(ArrayList<Message> arrayList) {
        if (messengerAdapter != null) {
            messengerAdapter.setData(arrayList);
        }
    }

    private void backHomeFriendFragment() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_READ_PERMISSION_CODE);
        } else {
            loadImages();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_READ_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                loadImages();
            } else {
                Toast.makeText(getActivity(), "Fail", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void loadImages() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        recyclerViewGallery.setLayoutManager(gridLayoutManager);
        recyclerViewGallery.setFocusable(false);
        List<String> listImage = ImageGallery.lisOfImages(getActivity());
        selectImageAdapter = new SelectImageAdapter(getContext(), listImage);
        recyclerViewGallery.setAdapter(selectImageAdapter);
        selectImageAdapter.setOnItemGalleryClick(new SelectImageAdapter.ClickItemGallery() {
            @Override
            public void onClickItemGallery(String image, int position) {
                if (selectImageAdapter.getListImg().size() > 0) {
                    btnCancel.setVisibility(View.VISIBLE);
                    btnSend.setVisibility(View.VISIBLE);
                } else {
                    btnCancel.setVisibility(View.GONE);
                    btnSend.setVisibility(View.GONE);
                }

            }
        });
    }

    private void loadSticker() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        recyclerViewSticker.setLayoutManager(gridLayoutManager);
        recyclerViewSticker.setFocusable(false);
        ArrayList listSticker = new ArrayList<>(AppConstants.stickers);
        stickerAdapter = new StickerAdapter(getContext(), listSticker);
        recyclerViewSticker.setAdapter(stickerAdapter);
        stickerAdapter.setOnItemStickerClick(new StickerAdapter.ClickItemSticker() {
            @Override
            public void onClickItemSticker(String sticker, int position) {
                mPresenter.sendSticker(sticker);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }
}



