package com.android.longhh.chat.ui.home.homefriend.tabrequest;

import androidx.annotation.NonNull;

import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class TabRequestPresenter <V extends TabRequestMvpView> extends BasePresenter<V>
        implements TabRequestMvpPresenter<V> {

    @Inject
    public TabRequestPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
    Users sender;
    @Override
    public void readSender() {
        getDataSender();
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_FRIEND_REQUEST)
                .child(getDataManager().getFirebaseUserId())
                .child(AppConstants.REFERENCE_REQUEST_SEND)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userArrayList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && getDataManager().getFirebaseUser()!=null) {
                                if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                    userArrayList.add(user);
                                }
                            }
                        }
                        getMvpView().readSender(userArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getDataSender() {
        getDataManager().getCurrentDataUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        sender = dataSnapshot.getValue(Users.class);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }
    @Override
    public void readReceiver() {
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_FRIEND_REQUEST)
                .child(getDataManager().getFirebaseUserId())
                .child(AppConstants.REFERENCE_REQUEST_RECEIVE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userArrayList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && getDataManager().getFirebaseUser()!=null) {
                                if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                    userArrayList.add(user);
                                }
                            }
                        }
                        getMvpView().readReceiver(userArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
    private void removeValueSender(Users users, String id, String table){
        Query query = getDataManager().getDatabaseReference()
                .child(AppConstants.REFERENCE_FRIEND_REQUEST)
                .child(id)
                .child(table)
                .orderByChild(AppConstants.FIRE_BASE_ID).equalTo(users.getId());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                    readSender();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void cancelRequest(Users users) {
        removeValueSender(users, getDataManager().getFirebaseUserId(), AppConstants.REFERENCE_REQUEST_SEND);
        removeValueSender(sender, users.getId(), AppConstants.REFERENCE_REQUEST_RECEIVE);
    }

    @Override
    public void agreeRequest(Users users) {
        removeValueSender(users, getDataManager().getFirebaseUserId(), AppConstants.REFERENCE_REQUEST_RECEIVE);
        removeValueSender(sender, users.getId(), AppConstants.REFERENCE_REQUEST_SEND);

        getDataManager().getDatabaseReference(AppConstants.REFERENCE_FRIENDS)
                .child(getDataManager().getFirebaseUserId())
                .push()
                .setValue(users);
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_FRIENDS)
                .child(users.getId())
                .push()
                .setValue(sender);
    }

    @Override
    public void disagreeRequest(Users users) {
        removeValueSender(users, getDataManager().getFirebaseUserId(), AppConstants.REFERENCE_REQUEST_RECEIVE);
        removeValueSender(sender, users.getId(), AppConstants.REFERENCE_REQUEST_SEND);
    }
}
