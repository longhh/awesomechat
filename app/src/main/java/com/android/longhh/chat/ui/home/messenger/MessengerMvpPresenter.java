package com.android.longhh.chat.ui.home.messenger;

import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.ui.base.MvpPresenter;

import java.util.ArrayList;

public interface MessengerMvpPresenter<V extends MessengerMvpView> extends MvpPresenter<V> {

    void setRoom(Room room);

    void sendMessage(String message);

    void sendImage(ArrayList<String> listImg);

    void sendSticker(String sticker);
}
