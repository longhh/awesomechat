package com.android.longhh.chat.ui.home.homefriend;

import com.android.longhh.chat.ui.base.MvpPresenter;

public interface HomeFriendMvpPresenter <V extends HomeFriendMvpView> extends MvpPresenter<V> {
    void readRequest();
}
