package com.android.longhh.chat.ui.home.homemessenger;

import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface HomeMessengerMvpView extends MvpView {
    void onSuccess(ArrayList<Room> roomArrayList);
}
