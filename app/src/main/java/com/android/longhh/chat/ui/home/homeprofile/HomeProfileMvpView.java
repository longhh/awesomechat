package com.android.longhh.chat.ui.home.homeprofile;

import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.MvpView;

public interface HomeProfileMvpView extends MvpView {
    void onSuccess(Users users, String email);
}
