package com.android.longhh.chat.ui.home.homefriend.tabfriend;

import com.android.longhh.chat.data.firebase.Users;

public class SortUser {
    private String Header;
    private Users users;
    private  boolean isSection;

    public SortUser(String header, Users users, boolean isSection) {
        Header = header;
        this.users = users;
        this.isSection = isSection;
    }

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public boolean isSection() {
        return isSection;
    }

    public void setSection(boolean section) {
        isSection = section;
    }
}
