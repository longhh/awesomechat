package com.android.longhh.chat.ui.register;

import com.android.longhh.chat.di.PerActivity;
import com.android.longhh.chat.ui.base.MvpPresenter;

@PerActivity
public interface RegisterMvpPresenter<V extends RegisterMvpView> extends MvpPresenter<V> {

    void onRegister(String username, String email, String password);

}
