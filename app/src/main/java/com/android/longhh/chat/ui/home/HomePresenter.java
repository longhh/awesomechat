package com.android.longhh.chat.ui.home;

import android.util.Log;

import androidx.annotation.NonNull;

import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.fcm.Token;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.badge.BadgeDrawable;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class HomePresenter<V extends HomeMvpView> extends BasePresenter<V>
        implements HomeMvpPresenter<V> {

    BadgeDrawable badgeDrawable;

    @Inject
    public HomePresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        getDataUser();
        updateToken();
    }

    private void getDataUser() {
        getDataManager().getCurrentDataUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Users user = dataSnapshot.getValue(Users.class);
                        getDataManager().setDataUser(user);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });

    }

    @Override
    public void readRooms() {
        getDataManager().getDatabaseReferenceChild(AppConstants.REFERENCE_ROOMS, getDataManager().getFirebaseUserId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        ArrayList<Room> rooms = new ArrayList<>();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Room room = dataSnapshot.getValue(Room.class);
                            if (room != null && !room.getLastMessage().equals("")) {
                                rooms.add(room);
                            }
                        }
                        int count = 0;
                        for (int i = 0; i < rooms.size(); i++){
                            if(rooms.get(i).getUnreadMessage() > 0){
                                count++;
                            }
                        }
                        getMvpView().readRooms(count);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        getMvpView().showMessage(error.getMessage());
                    }
                });
    }

    @Override
    public void readRequest() {
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_FRIEND_REQUEST)
                .child(getDataManager().getFirebaseUserId())
                .child(AppConstants.REFERENCE_REQUEST_RECEIVE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userArrayList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && getDataManager().getFirebaseUser()!=null) {
                                if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                    userArrayList.add(user);
                                }
                            }
                        }
                        getMvpView().readRequest(userArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void updateToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(HomeFragment.TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }
                        // Get new FCM registration token
                        String token = task.getResult();
                        //Log
                        Log.d(HomeFragment.TAG, token);
                        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(AppConstants.REFERENCE_TOKENS);
                        Token token1 = new Token(token);
                        databaseReference.child(firebaseUser.getUid()).setValue(token1);
                    }
                });
    }
}
