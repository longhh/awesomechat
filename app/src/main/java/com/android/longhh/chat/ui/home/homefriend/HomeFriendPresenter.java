package com.android.longhh.chat.ui.home.homefriend;

import androidx.annotation.NonNull;

import com.android.longhh.chat.data.DataManager;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.BasePresenter;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;

public class HomeFriendPresenter <V extends HomeFriendMvpView> extends BasePresenter<V>
        implements HomeFriendMvpPresenter<V> {

    @Inject
    public HomeFriendPresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void readRequest() {
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.REFERENCE_FRIEND_REQUEST)
                .child(getDataManager().getFirebaseUserId())
                .child(AppConstants.REFERENCE_REQUEST_RECEIVE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userArrayList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && getDataManager().getFirebaseUser()!=null) {
                                if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                    userArrayList.add(user);
                                }
                            }
                        }
                        getMvpView().readRequest(userArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }
}
