package com.android.longhh.chat.ui.home.messenger;

import android.content.Context;

import com.android.longhh.chat.data.firebase.Message;
import com.android.longhh.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface MessengerMvpView extends MvpView {
    Context getContext();
    void onSuccess(ArrayList<Message> arrayList);
}
