package com.android.longhh.chat.ui.home.homefriend.tabfriend;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.di.component.ActivityComponent;
import com.android.longhh.chat.ui.base.BaseFragment;
import com.android.longhh.chat.ui.home.homefriend.HomeFriendFragment;
import com.android.longhh.chat.ui.home.messenger.MessengerFragment;
import com.android.longhh.chat.utils.AppConstants;
import java.util.ArrayList;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TabFriendFragment extends BaseFragment implements TabFriendMvpView {

    public static final String TAG = TabFriendFragment.class.getSimpleName();

    @BindView(R.id.recycler_view_tab_user)
    RecyclerView recyclerViewTabFriend;

    TabFriendAdapter adapter;

    @Inject
    TabFriendMvpPresenter<TabFriendMvpView> mPresenter;

    public static TabFriendFragment newInstance(Bundle args) {
        TabFriendFragment fragment = new TabFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static TabFriendFragment newInstance() {
        Bundle args = new Bundle();
        TabFriendFragment fragment = new TabFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_user, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }


    @Override
    protected void setUp(View view) {
        setInitAdapter();
        mPresenter.readUser();
        HomeFriendFragment homeFriendFragment = (HomeFriendFragment) getParentFragment();
        if (homeFriendFragment != null) {
            homeFriendFragment.setOnTextChanged(new HomeFriendFragment.OnTextChanged() {
                @Override
                public void onTextChanged(String word) {
                    if (word != null) {
                        mPresenter.searchUser(word);
                    }

                }
            });
        }
        recyclerViewTabFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }

    private void setInitAdapter() {
        recyclerViewTabFriend.setHasFixedSize(true);
        recyclerViewTabFriend.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new TabFriendAdapter(getContext(), new ArrayList<>());
        recyclerViewTabFriend.setAdapter(adapter);
    }

    @Override
    public void onSuccess(ArrayList<SortUser> usersArrayList) {
        if (adapter != null) {
            adapter.setData(usersArrayList);
            adapter.setOnClick(new TabFriendAdapter.IOnClick() {
                @Override
                public void onItemCLick(int i) {
                    mPresenter.checkExistsRoom(usersArrayList.get(i).getUsers());
                }
            });
        }
    }

    @Override
    public void openChatRoom(Room room) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEY_ROOM, room);
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_left, R.anim.slide_right, R.anim.slide_left, R.anim.slide_right)
                    .add(R.id.frameContainer, MessengerFragment.newInstance(bundle), MessengerFragment.TAG)
                    .addToBackStack(MessengerFragment.TAG)
                    .commit();
        }
    }

}

