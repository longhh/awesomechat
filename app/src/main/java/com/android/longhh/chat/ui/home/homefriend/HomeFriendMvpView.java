package com.android.longhh.chat.ui.home.homefriend;

import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface HomeFriendMvpView extends MvpView {
    void readRequest(ArrayList<Users> userArrayList);
}
