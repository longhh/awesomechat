package com.android.longhh.chat.ui.home.messenger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.ui.home.homemessenger.HomeMessengerAdapter;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectImageAdapter extends RecyclerView.Adapter<SelectImageAdapter.MyViewHolder> {

    private Context context;
    private List<String> list;
    private ArrayList<String> listImg = new ArrayList<>();
    public ClickItemGallery clickItemGallery;


    public SelectImageAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    public ArrayList<String> getListImg(){
        return listImg;
    }
    public void clearList(){
        listImg.clear();
    }

    public interface ClickItemGallery {
        void onClickItemGallery (String image, int position);
    }

    public void setOnItemGalleryClick (ClickItemGallery clickItemGallery) {
        this.clickItemGallery = clickItemGallery;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_select_image, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        String image = list.get(i);
        myViewHolder.bindView(image);
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        } else {
            return list.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_from_gallery)
        ImageView imgFromGallery;

        @BindView(R.id.check_select)
        TextView checkSelect;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(String image){
            Glide.with(context).load(new File(image)).into(imgFromGallery);
            imgFromGallery.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View view) {
                    if (checkSelect.getVisibility() == View.VISIBLE) {
                        checkSelect.setVisibility(View.GONE);
                    } else {
                        checkSelect.setVisibility(View.VISIBLE);
                    }
                    if (listImg.contains(image)){
                        listImg.remove(image);
                    } else {
                        listImg.add(image);
                    }
                    if(clickItemGallery != null){
                        clickItemGallery.onClickItemGallery(image, getAdapterPosition());
                    }
                    checkSelect.setText("" + listImg.size());
                }
            });
        }
    }
}
