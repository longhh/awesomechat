package com.android.longhh.chat.ui.home.messenger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Message;
import com.android.longhh.chat.utils.AppConstants;
import com.android.longhh.chat.utils.DateUtils;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessengerAdapter extends RecyclerView.Adapter<MessengerAdapter.MyViewHolder> {
    public static final int MSG_TYPE_RIGHT = 1;
    public static final int MSG_TYPE_LEFT = 0;
    public static final int IMAGE_LEFT = 2;
    public static final int IMAGE_RIGHT = 3;
    private final Context context;
    private final String imageURL;
    private ArrayList<Message> data;
    FirebaseUser firebaseUser;
    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdfFull = new SimpleDateFormat(AppConstants.TimeFormat.FULL);

    public MessengerAdapter(Context context, ArrayList<Message> data, String imageURL) {
        this.context = context;
        this.data = data;
        this.imageURL = imageURL;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(ArrayList<Message> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public String getTime(int pos) throws ParseException {
        Date time = sdfFull.parse(data.get(pos).getTime());
        DateUtils timeMess = new DateUtils(time);
        DateUtils calendar = new DateUtils(Calendar.getInstance().getTime());

        if (timeMess.getMonth().equals(calendar.getMonth())) {
            int i = Integer.parseInt(timeMess.getDay()) - Integer.parseInt(calendar.getDay());
            if (i == 0) {
                return context.getString(R.string.now);
            } else if (i == 1) {
                return context.getString(R.string.yesterday);
            } else if (i > 1){
                return timeMess.getDate();
            } else {
                return context.getString(R.string.now);
            }
        } else {
            return timeMess.getDate();
        }
    }

    @NonNull
    @Override
    public MessengerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i == MSG_TYPE_RIGHT) {
            view = LayoutInflater.from(context).inflate(R.layout.item_mess_right, viewGroup, false);
        } else if (i == MSG_TYPE_LEFT) {
            view = LayoutInflater.from(context).inflate(R.layout.item_mess_left, viewGroup, false);
        } else if (i == IMAGE_RIGHT) {
            view = LayoutInflater.from(context).inflate(R.layout.item_image_right, viewGroup, false);
        } else
            view = LayoutInflater.from(context).inflate(R.layout.item_image_left, viewGroup, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MessengerAdapter.MyViewHolder myViewHolder, int i) {
        Message message = data.get(i);
        try {
            myViewHolder.bindView(message);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @BindView(R.id.txt_show_mess)
        TextView txtShowMess;
        @BindView(R.id.img_circle_show_mess)
        CircleImageView imgCircleShowMess;
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.show_mess_image)
        ImageView showImage;

        public void bindView(Message message) throws ParseException {
            if (message.getContent().startsWith("https://")) {
                Glide.with(context).load(message.getContent()).into(showImage);
            } else {
                txtShowMess.setText(message.getContent());
            }
            if (imageURL != null) {
                if (imageURL.equals(AppConstants.FIRE_BASE_DEFAULT))
                    Glide.with(context).load(R.drawable.ic_avt).into(imgCircleShowMess);
                else
                    Glide.with(context).load(imageURL).into(imgCircleShowMess);
            }
            Date time = sdfFull.parse(message.getTime());
            DateUtils timeMess = new DateUtils(time);
            if (getAdapterPosition() == data.size() - 1) {
                txtTime.setVisibility(View.VISIBLE);
                txtTime.setText(timeMess.getHour());
            } else {
                txtTime.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (data.get(position).getSenderId().equals(firebaseUser.getUid())) {
            if (data.get(position).getContent().startsWith("https://")) {
                return IMAGE_RIGHT;
            } else {
                return MSG_TYPE_RIGHT;
            }
        } else {
            if (data.get(position).getContent().startsWith("https://")) {
                return IMAGE_LEFT;
            } else {
                return MSG_TYPE_LEFT;
            }
        }
    }
}
