package com.android.longhh.chat.ui.home.homefriend.tabrequest;

import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.ui.base.MvpPresenter;
import com.android.longhh.chat.ui.base.MvpView;

public interface TabRequestMvpPresenter<V extends TabRequestMvpView & MvpView> extends MvpPresenter<V> {
    void readSender();

    void readReceiver();

    void cancelRequest(Users users);

    void agreeRequest(Users users);

    void disagreeRequest(Users users);
}
