package com.android.longhh.chat.ui.home.homefriend.tabfriend;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.longhh.chat.R;
import com.android.longhh.chat.data.firebase.Users;
import com.android.longhh.chat.utils.AppConstants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TabFriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private List<SortUser> sortUserList;

    public static final int SECTION_VIEW = 1;
    public static final int CONTENT_VIEW = 2;

    public interface IOnClick {
        void onItemCLick(int i);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(ArrayList<SortUser> userList){
        this.sortUserList = userList;
        notifyDataSetChanged();
    }
    public IOnClick iOnClick;

    void setOnClick(IOnClick iOnClick) {
        this.iOnClick = iOnClick;
    }

    public TabFriendAdapter(Context context, List<SortUser> sortUserList) {
        this.context = context;
        this.sortUserList = sortUserList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i == SECTION_VIEW) {
            view = LayoutInflater.from(context).inflate(R.layout.item_tab_user_header, viewGroup, false);
            return new MyViewHolderHead(view);
        }
        view = LayoutInflater.from(context).inflate(R.layout.item_tab_friend, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        SortUser user = sortUserList.get(i);
        switch (viewHolder.getItemViewType()) {
            case SECTION_VIEW:
                MyViewHolderHead myViewHolderHead = (MyViewHolderHead) viewHolder;
                myViewHolderHead.bindView(user);
                break;
            case CONTENT_VIEW:
                MyViewHolder myViewHolder = (MyViewHolder) viewHolder;
                myViewHolder.bindView(user, iOnClick);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (sortUserList.get(position).isSection()) {
            return SECTION_VIEW;
        } else {
            return CONTENT_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        return sortUserList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_username)
        TextView txtUsername;
        @BindView(R.id.img_circle_friend)
        CircleImageView imgCircleFriend;
        @BindView(R.id.item_tab_friend)
        ConstraintLayout itemTabFriend;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("CheckResult")
        public void bindView(SortUser user, IOnClick iOnClick) {
            Typeface typeBold = ResourcesCompat.getFont(context, R.font.lato_bold);
            txtUsername.setTypeface(typeBold);
            txtUsername.setText(user.getUsers().getUsername());
            itemTabFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (iOnClick != null) {
                        iOnClick.onItemCLick(getAdapterPosition());
                    }
                }
            });

            if (user.getUsers().getImageURL().equals(AppConstants.FIRE_BASE_DEFAULT)) {
                Glide.with(context).load(R.drawable.ic_avt).circleCrop();
            } else {
                Glide.with(context).load(user.getUsers().getImageURL()).circleCrop().into(imgCircleFriend);
            }
        }
    }

    public class MyViewHolderHead extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_header)
        TextView txtHeader;

        public MyViewHolderHead(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
        public void bindView(SortUser user) {
            Typeface typeBold = ResourcesCompat.getFont(context, R.font.lato_bold);
            txtHeader.setTypeface(typeBold);
            txtHeader.setText(user.getHeader());
        }
    }
}