package com.android.longhh.chat.ui.home.homefriend.tabfriend;

import com.android.longhh.chat.data.firebase.Room;
import com.android.longhh.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface TabFriendMvpView extends MvpView {
    void onSuccess(ArrayList<SortUser> usersArrayList);

    void openChatRoom(Room room);
}
