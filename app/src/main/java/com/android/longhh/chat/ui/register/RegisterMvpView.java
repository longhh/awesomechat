package com.android.longhh.chat.ui.register;

import com.android.longhh.chat.ui.base.MvpView;

public interface RegisterMvpView extends MvpView {
    void successRegister();

    void fieldRegister();

}